#include <vector>

#include "Pipeline.hpp"
#include "TDD2Ftab2.hpp"
#include "Utils.hpp"

#define FILE_COUNT 7

using namespace std;

float IntegTHR(TH1F *h, float threshold = 50, float delay = 10,
               TGraph *g = new TGraph()) {
    auto *tdd2ftab = new TDD2FTab2(40, 1, 600);
    // auto *c = new TCanvas("c1", "", 600, 600);

    g->Clear();
    tdd2ftab->convert(h, g, threshold);
    // h->Draw();
    // g->Draw("*");

    float gInteg = Utils::integrate(g), hInteg = h->Integral();
    float err = (hInteg - gInteg) / hInteg * 100;
    // printf("gInteg: %f, hInteg: %f, err: %f\n", gInteg, hInteg, err);
    return err;
}

void Test2(float threshold = 10, float delay = 10, const char *fStr = "pol1") {
    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < FILE_COUNT; i++)
        names->push_back(Form("data/signals%i.root", i));
    Provider<TH1F> *provider = new TObjectProvider<TH1F>(names);

    vector<float> diffsA;
    vector<float> amps;

    TH1F *errsB = new TH1F("errsB", "errs", 100, 0, 100);
    TH1F *errsN = new TH1F("errsN", "errs", 100, 10, 50);

    TH2F *diffs = new TH2F("diffs", "diffs", 20, 60, 180, 20, 60, 180);
    TH1F *ampDiffs = new TH1F("ampDiffs", "ampDiffs", 50, -25, 25);

    int cnt = 0;
    while (provider->hasMore()) {
        TGraph *g = new TGraph();

        auto *h = provider->get();
        // auto *can = new TCanvas();
        // h->Draw();
        // can->WaitPrimitive();
        // can->Close();

        if (!h) continue;

        float m = h->GetMinimum();
        for (int i = 0; i < h->GetSize(); i++)
            h->SetBinContent(i, h->GetBinContent(i) - m);

        // if (!Utils::validate(h)) continue;

        float err = IntegTHR(h, threshold, delay, g);
        errsB->Fill(err);
        errsN->Fill(err);

        double x, y;
        g->GetPoint(0, x, y);
        float diff = y - threshold;
        amps.push_back(h->GetMaximum());
        diffsA.push_back(diff);
        cnt++;

        diffs->Fill(diff, h->GetMaximum());

        delete g;
    }
    printf("%i\n", cnt);

    // auto *c1 = new TCanvas("c1", "c1", 1200, 600);
    // c1->Divide(2);
    // c1->cd(1);
    // errsB->Draw();
    // c1->cd(2);
    // errsN->Draw();

    gStyle->SetOptFit(1111);
    auto *c2 = new TCanvas("c2", "c2", 1200, 600);
    c2->Divide(2);
    c2->cd(1);
    diffs->Fit(fStr, "M");
    TF1 *fn = diffs->GetFunction(fStr);
    fn->Print();
    diffs->Draw("colz");

    for (int i = 0; i < cnt; i++) {
        float ampC = fn->Eval(diffsA[i]);
        ampDiffs->Fill((amps[i] - ampC) / amps[i] * 100);
    }
    c2->cd(2);
    ampDiffs->Fit("gaus", "MQ");
    ampDiffs->Draw();

    c2->SaveAs(Form("/tmp/root/%2.0f|%2.0f-%s.png", threshold, delay, fStr));
}

#ifdef __GNUC__
int main(int argc, char **argv) {
    TApplication App(argv[0], &argc, argv);
    Test2();
    return 0;
}
#endif