#pragma once

#include "Abstracts.hpp"
#include "Common.hpp"
#include "Utils.hpp"

#define TEMPLATE 0
#define COUNT 2

class IntegralCurveFinder : public Consumer<TObjectPair<TH1F, TGraph>, Result> {
   private:
    static Result *SimpleAlgorithm(TH1F *h, TGraph *g) {
        double integ = Utils::integrate(g);
        // double integ = g->Integral();
        double x, y, xMax, yMax = -1;

        for (int i = 0; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);

            if (y > yMax) {
                xMax = x;
                yMax = y;
            }
        }

        Result *res = new Result();
        res->graph = integ;
        res->orig = h->Integral();
        res->err = (res->orig - res->graph) / res->orig * 100;
        res->amp = h->GetMaximum();

        res->type = FinderType::Integ;

        return res;
    }

    Result *consumeInt(TObjectPair<TH1F, TGraph> *p,
                       bool threadSafe = false) override {
        Result *ret = nullptr;
        if (p && p->e1 && p->e2) {
            ret = SimpleAlgorithm(p->e1, p->e2);
            // if (abs(ret->err) > 50) {
            //     TCanvas *c = new TCanvas();
            //     // p->e2->Print();
            //     p->e1->SaveAs("test.C");
            //     printf("%lf%% -> %lf - %lf\n", ret->err, ret->orig,
            //     ret->graph);

            //     p->e2->SetMarkerColor(kRed);
            //     p->e2->Draw("AL*");
            //     p->e1->Draw("same");
            //     c->Update();
            //     c->WaitPrimitive();
            //     c->Close();
            // }
        }

        return ret;
    }

   public:
    // IntegralCurveFinder() {

    // }
};