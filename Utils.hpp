#pragma once

#include <cstdio>
#include <limits>

#include "Abstracts.hpp"

class Utils {
   public:
    static TH1F* extractPeak(TH1F* h) {
        float sum = h->GetBinContent(1);
        int cnt = 1;

        for (; abs((sum / cnt) - h->GetBinContent(cnt + 2)) < 2.5f &&
               cnt < h->GetSize();
             cnt++)
            sum += h->GetBinContent(cnt + 1);

        float avgMin = sum / cnt;
        TH1F* h2 = new TH1F("h2", h->GetTitle(), 300, cnt, cnt + 300);

        for (int j = 0; j < 300 && cnt + j - 2 < h->GetSize(); j++) {
            h2->SetBinContent(j, h->GetBinContent(j + cnt - 2) - avgMin);
        }

        return h2;
    }
    static TH1F* extractPeakInPlace(TH1F*& h) {
        TH1F* h2 = extractPeak(h);
        const char* name = h->GetName();
        delete h;
        h2->SetName(name);
        h = h2;
        return h;
    }

    static TH1F* extractTemplatePeak(TH1F* h) {
        float sum = h->GetBinContent(0);
        int cnt = 1;

        for (; abs((sum / cnt) - h->GetBinContent(cnt + 1)) < 0.0015f &&
               cnt < h->GetSize();
             cnt++) {
            sum += h->GetBinContent(cnt);
        }

        float avgMin = sum / cnt;
        TH1F* h2 = new TH1F("h2", h->GetTitle(), 300, cnt, cnt + 300);

        for (int j = 0; j < 300 && cnt + j - 2 < h->GetSize(); j++) {
            h2->SetBinContent(j, h->GetBinContent(j + cnt - 2) - avgMin);
        }

        h2->SetName(h->GetName());

        return h2;
    }

    static TGraph* extractPeak(TGraph* g) {
        double prevX, prevY;
        double x, y;
        g->GetPoint(0, x, y);
        float sum = y;
        int cnt = 1;

        for (g->GetPoint(cnt, x, y); abs((sum / cnt) - y) < 2.5f; cnt++) {
            sum += y;
            prevX = x;
            prevY = y;
            g->GetPoint(cnt + 1, x, y);
        }

        float avgMin = sum / cnt;
        TGraph* g2 = new TGraph();

        double xStart = prevX;

        int j;
        for (j = 0; abs(prevX - xStart) < 300 && cnt + j - 2 < g->GetN(); j++) {
            g2->SetPoint(j, prevX, prevY - avgMin);
            prevX = x;
            prevY = y;
            g->GetPoint(cnt + j, x, y);
        }
        g2->SetPoint(j, x, y - avgMin);

        g2->SetTitle(g->GetTitle());

        return g2;
    }

    static TGraph* extractPeakInPlace(TGraph*& g) {
        TGraph* g2 = extractPeak(g);
        const char* name = g->GetName();
        delete g;
        g2->SetName(name);
        g = g2;

        return g;
    }

    static TObjectPair<TH1F, TGraph>* extractPeakInPlace(
        TObjectPair<TH1F, TGraph>* p) {
        if (!p) return nullptr;

        auto* tmp = p->e1;
        const char* name = tmp->GetName();
        p->e1 = Utils::extractPeak(p->e1);
        delete tmp;
        p->e1->SetName(name);

        p->e2 = Utils::extractPeakInPlace(p->e2);

        return p;
    }

    static void drawTwoPeaks(const char* name, TDirectory* hists,
                             TDirectory* graphs, TCanvas* c = new TCanvas()) {
        TH1F* h = (TH1F*)hists->Get(name);
        TGraph* g = (TGraph*)graphs->Get(name);

        if (g == NULL) g = (TGraph*)graphs->Get(Form("%s-inter", name));

        TH1F* hPeak = extractPeak(h);
        TGraph* gPeak = extractPeak(g);
        delete h, g;

        hPeak->SetLineColor(kBlue);

        gPeak->SetLineColor(kWhite);
        gPeak->SetMarkerColor(kRed);

        double x = 0, y = 0;

        c->cd(1);
        hPeak->Draw();
        gPeak->Draw("*");
    }

    static TH1F* subtractBaselineInPlace(TH1F* h, float threshold = 2.5f) {
        int i = 0;
        float cur = h->GetBinContent(i++), base = 0;

        do {
            base += cur;
            cur = h->GetBinContent(i++);
        } while (abs(base / (i - 1) - cur) < threshold);

        base /= i - 1;
        printf("Base: %f\n", base);

        for (i = 0; i < h->GetSize(); i++) {
            float bin = h->GetBinContent(i) - base;
            h->SetBinContent(i, bin);
        }

        return h;
    }

    // static bool validatePeak(TH1F* peak) { return peak->GetMaximum() > 20.0f;
    // }

    static TH1F* zeroAlignInPlace(TH1F* h) {
        float min = h->GetMinimum();

        for (int i = 0; i < h->GetSize(); i++) {
            float bin = h->GetBinContent(i) - min;
            h->SetBinContent(i, bin);
        }
        return h;
    }

    static TH1F* zeroAlignAndNormalizeInPlace(TH1F* h) {
        float min = h->GetMinimum();
        float scale = 1 / (h->GetMaximum() - min);

        for (int i = 0; i < h->GetSize(); i++) {
            float bin = h->GetBinContent(i) - min;
            h->SetBinContent(i, bin * scale);
        }
        return h;
    }

    static TH1F* getAmplitudeHistogram(TDirectory* dir) {
        TList* l = dir->GetListOfKeys();

        int n = l->GetSize();
        TH1F* res = new TH1F("res", "", n, 0, n);

        for (int i = 0; i < n; i++) {
            TH1F* h = (TH1F*)dir->Get(l->At(i)->GetName());
            res->SetBinContent(i, h->GetMaximum());
            delete h;
        }

        return res;
    }

    static float** sortPeaks(TH1F* h) {
        TH1F* hCopy = new TH1F(*h);
        int n = h->GetSize(), cur = 0;
        float* indices = new float[n];
        float* values = new float[n];

        for (int i = 0; i < n; i++) {
            indices[cur] = hCopy->GetMinimumBin();
            values[cur++] = hCopy->GetMinimum();
            hCopy->SetBinContent(hCopy->GetMinimumBin(),
                                 std::numeric_limits<float>::max());
        }

        delete hCopy;

        float** ret = new float*[2];
        ret[0] = indices;
        ret[1] = values;
        return ret;
    }

    static void closeAllCanvases() {
        for (TObject* c : *(gROOT->GetListOfCanvases())) {
            ((TCanvas*)c)->Close();
        }
    }

    static bool validate(TGraph* g) {
        // g->RemovePoint(g->GetN() - 1);
        double x, y;
        bool res = true;

        for (int i = 0; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);
            res = res && y > -5.5;
        }

        return abs(y) < 26 && res;
    }

    static bool validate(TH1F* h) {
        if (!h) return false;

        // h->Delete();

        float integ = h->Integral();
        if (integ > 5000 && integ < 30000)
            return true;
        else
            return false;
    }

    static TH1F** getTemplates(TDirectory* temps, int& n, int limit = -1) {
        TList* l = temps->GetListOfKeys();
        n = (limit == -1 || limit > l->GetSize()) ? l->GetSize() : limit;

        TH1F** arr = new TH1F*[n];

        for (int i = 0; i < n; i++) {
            arr[i] = (TH1F*)temps->Get(l->At(i)->GetName());
        }

        return arr;
    }

    static TH1F** getTemplates(TDirectory* temps, int& n, float*& amps,
                               int limit = -1) {
        TList* l = temps->GetListOfKeys();
        n = (limit == -1 || limit > l->GetSize()) ? l->GetSize() : limit;

        TH1F** arr = new TH1F*[n];
        amps = new float[n];

        for (int i = 0; i < n; i++) {
            const char* name = l->At(i)->GetName();
            arr[i] = (TH1F*)temps->Get(name);
            sscanf(name, "normalized_group_%f", &amps[i]);
        }

        return arr;
    }

    static TH1F** getTemplates(TFile* temps, int& n, int limit = -1) {
        return getTemplates((TDirectory*)temps, n);
    }

    static float integrate(TGraph* g) {
        float integ = 0;

        int n = g->GetN();

        for (int i = 1; i < n; i++) {
            double x1, y1, x2, y2;
            g->GetPoint(i - 1, x1, y1);
            g->GetPoint(i, x2, y2);
            float step = (y1 + y2) * (x2 - x1) / 2;
            integ += step;
        }

        return integ;
    }

    static void getMaxPoint(double& xMax, double& yMax, TGraph* g) {
        double x, y;
        if (g->GetN() == 0) {
            xMax = -1;
            yMax = -1;
        } else {
            g->GetPoint(0, xMax, yMax);

            for (int i = 1; i < g->GetN(); i++) {
                g->GetPoint(i, x, y);

                if (yMax < y) {
                    xMax = x;
                    yMax = y;
                }
            }
        }
    }

    static void getMaxPointAdjusted(double& xMax, double& yMax, TGraph* g) {
        double xStart = -1, x, y;
        if (g->GetN() == 0) {
            xMax = -1;
            yMax = -1;
        } else {
            g->GetPoint(0, xMax, yMax);

            for (int i = 1; i < g->GetN(); i++) {
                g->GetPoint(i, x, y);
                if (xStart == -1) xStart = x;

                if (yMax < y) {
                    xMax = x;
                    yMax = y;
                }
            }

            xMax -= xStart;
        }
    }

    static void getMaxPointAdjusted(double& xMax, double& yMax, TH1F* h) {
        if (!h) {
            xMax = yMax = -1;
        } else {
            double offset = h->GetXaxis()->GetXmin();
            xMax = h->GetMaximumBin() + offset;
            yMax = h->GetMaximum();
        }
    }

    static TH1F* alignLeftInPlace(TH1F* h) {
        auto* axis = h->GetXaxis();
        double len = axis->GetXmax() - axis->GetXmin();
        axis->SetLimits(0, len);
        return h;
    }

    static TGraph* alignLeftInPlace(TGraph* g) {
        auto* axis = g->GetXaxis();
        double len = axis->GetXmax() - axis->GetXmin();
        axis->SetLimits(0, len);

        double x, y, xMin = -1;
        for (int i = 0; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);
            if (xMin == -1)
                xMin = x;
            else
                g->SetPoint(i, x - xMin, y);
        }

        return g;
    }

    static TH1F* scaleHistogram(TH1F* h, float scale) {
        TH1F* h2 = new TH1F(*h);
        for (int i = 0; i < h2->GetSize(); i++) {
            float bin = h2->GetBinContent(i) * scale;
            h2->SetBinContent(i, bin);
        }

        return h2;
    }

    static TH1F* scaleHistogramInPlace(TH1F* h, float scale) {
        for (int i = 0; i < h->GetSize(); i++) {
            float bin = h->GetBinContent(i) * scale;
            h->SetBinContent(i, bin);
        }

        return h;
    }

    static TObjArray* fitSlicesY(TH2* h) {
        TObjArray* arr = new TObjArray();
        h->FitSlicesY(0, 0, -1, 0, "QNR", arr);
        return arr;
    }

    static TObjArray* fitSlicesY(TH2* h, TH1D*& hM, TH1D*& hS,
                                 float rangePos = 9999,
                                 float rangeNeg = -9999) {
        TObjArray* arr = new TObjArray();
        h->FitSlicesY(0, 0, -1, 0, "QNR", arr);
        hM = (TH1D*)arr->At(1);
        hS = (TH1D*)arr->At(2);

        for (int i = 0; i < hM->GetSize(); i++) {
            float bin = hM->GetBinContent(i);
            if (rangePos != 9999 && bin > rangePos)
                bin = rangePos;
            else if (rangeNeg != -9999 && bin < rangeNeg)
                bin = rangeNeg;
            hM->SetBinContent(i, bin);
        }
        for (int i = 0; i < hS->GetSize(); i++) {
            float bin = hS->GetBinContent(i);
            if (rangePos != 9999 && bin > rangePos)
                bin = rangePos;
            else if (rangeNeg != -9999 && bin < rangeNeg)
                bin = rangeNeg;
            hS->SetBinContent(i, bin);
        }

        hM->SetName(Form("%s-mean", h->GetName()));
        hM->SetTitle(hM->GetName());
        hS->SetName(Form("%s-sigma", h->GetName()));
        hS->SetTitle(hS->GetName());
        return arr;
    }

    static double getMean(TH1F* h) { return h->GetSum() / h->GetSize(); }

    static double getMean(TGraph* g) {
        double x, y, sum;
        sum = 0;
        for (int i = 0; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);
            sum += y;
        }

        return sum / g->GetN();
    }

    static double getMeanWeighted(TGraph* g) {
        double x, y, xPrev, sum;
        sum = 0;
        if (!g || g->GetN() == 0) return 0;
        g->GetPoint(0, x, y);

        for (int i = 1; i < g->GetN(); i++) {
            xPrev = x;
            g->GetPoint(i, x, y);
            sum += y * (x - xPrev);
        }

        return sum / g->GetN();
    }
};