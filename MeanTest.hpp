#pragma once

#include "Abstracts.hpp"
#include "Common.hpp"
#include "TDD2FTab.hpp"
#include "Utils.hpp"

#define TEMPLATE 0
#define COUNT 2

class MeanTest : public Consumer<TH1F, Result> {
   private:
    TH1F **templates;
    int nTemplates;
    float *amps;

    int i = 0;
    TH2F *hist;
    TFile *out = new TFile("meanOutTest.root", "RECREATE");
    TCanvas *can;

    Result *meanAlgorithm(TH1F *h, bool verbosity = false) {
        float amp = h->GetMaximum();
        TH1F *t = templates[getClosest(amps, nTemplates, amp)];
        TGraph *g = new TGraph(h);
        t = Utils::extractTemplatePeak(t);

        float hI = h->Integral();
        float tI = t->Integral();
        float scale = Utils::getMean(h) / Utils::getMean(t);
        float resI = tI * scale;
        float errS = (hI - resI) / hI * 100;

        if (verbosity) {
            can = (can) ? can : new TCanvas("can", "can", 1200, 1200);
            can->Clear();
            TH1F *tScaled = (TH1F *)t->Clone("tScaled");
            h->SetLineColor(kBlue);
            tScaled->Scale(scale);
            tScaled->SetLineColor(kRed);
            can->Divide(2, 2);
            can->cd(1);
            h->DrawClone();
            can->cd(2);
            t->DrawClone();
            can->cd(3);
            tScaled->Draw("same,hist");
            auto *a = h->GetXaxis();
            tScaled->GetXaxis()->SetLimits(a->GetXmin() - 4, a->GetXmax() - 4);
            printf("tScaled=%i, h=%i\n", tScaled->GetSize(), h->GetSize());
            h->DrawClone("same");
        }

        Result *res = new Result();
        res->graph = resI;
        res->orig = h->Integral();
        res->err = errS;
        res->amp = h->GetMaximum();

        if (verbosity) {
            printf("MeanH = %.2f, MeanT = %.2f\n", Utils::getMean(h),
                   Utils::getMean(t));
            printf(
                "Amp: %.2f, IOrig: %.2f, ITemp: %.2f, ITempScaled: %.2f, "
                "IScale: %.2f, Err: %.5f%%\n",
                amp, res->orig, t->Integral(), res->graph, scale, res->err);
        }

        g->SetMarkerColor(kRed);
        g->SetLineColor(kRed);

        auto *dir = out->mkdir(Form("%i", i++));
        dir->cd();
        g->Write();
        auto *tmp = Utils::scaleHistogram(t, scale);
        tmp->Write();

        res->type = FinderType::Mean;

        if (hist) hist->Fill(res->amp, res->err);

        g->Delete();
        h->Delete();
        tmp->Delete();

        return res;
    }

    Result *consumeInt(TH1F *h, bool threadSafe = false) override {
        Result *ret = nullptr;

        if (h) {
            ret = meanAlgorithm(h);
        }

        return ret;
    }

    int getClosest(float *arr, int n, float v) {
        int i;
        for (int i = 1; i < n; i++) {
            if (arr[i] > v) return i - 1;
        }
        return n - 1;
    }

   public:
    MeanTest(TH1F **templates, float *amps, TH2F *hist = nullptr) {
        this->templates = templates;
        this->amps = amps;

        this->hist = hist;
    }
};
