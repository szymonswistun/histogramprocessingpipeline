#include "ResultProducer.hpp"

using namespace std;

void ResultProducer(float freq = 20) {
    printf("Freq: %.1f\n", freq);

    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < 7; i++)
        names->push_back(Form("data/signals%i.root", i));

    Provider<TObjectPair<TH1F, TGraph>> *provider = new Preprocessor(
        new Interpolator(freq, new TObjectProvider<TH1F>(names)));

    double errMin = -300, errMax = 50;
    int xBin = 100, yBin = TMath::Nint(TMath::Ceil(
                        errMax - errMin));  // abs(errMin) + abs(errMax);

    TH2F *ampHS = new TH2F("ampHS", Form("Amp - error histogramS-%.0f", freq),
                           xBin, 20, 200, yBin, errMin, errMax);
    ampHS->SetXTitle("Amplitude");
    ampHS->SetYTitle("Error");
    TH2F *qHS = new TH2F("qHS", Form("Q - error histogramS-%.0f", freq), xBin,
                         4000, 30000, yBin, errMin, errMax);
    qHS->SetXTitle("Charge");
    qHS->SetYTitle("Error");

    TH2F *ampHR = new TH2F("ampHR", Form("Amp - error histogramR-%.0f", freq),
                           xBin, 20, 200, yBin, errMin, errMax);
    ampHR->SetXTitle("Amplitude");
    ampHR->SetYTitle("Error");
    TH2F *qHR = new TH2F("qHR", Form("Q - error histogramR-%.0f", freq), xBin,
                         4000, 30000, yBin, errMin, errMax);
    qHR->SetXTitle("Charge");
    qHR->SetYTitle("Error");

    TH2F *ampHI = new TH2F("ampHI", Form("Amp - error histogramI-%.0f", freq),
                           xBin, 20, 200, yBin, errMin, errMax);
    ampHI->SetXTitle("Amplitude");
    ampHI->SetYTitle("Error");
    TH2F *qHI = new TH2F("qHI", Form("Q - error histogramI-%.0f", freq), xBin,
                         4000, 30000, yBin, errMin, errMax);
    qHI->SetXTitle("Charge");
    qHI->SetYTitle("Error");

    TH2F *ampHM = new TH2F("ampHM", Form("Amp - error histogramM-%.0f", freq),
                           xBin, 20, 200, yBin, errMin, errMax);
    ampHM->SetXTitle("Amplitude");
    ampHM->SetYTitle("Error");
    TH2F *qHM = new TH2F("qHM", Form("Q - error histogramM-%.0f", freq), xBin,
                         4000, 30000, yBin, errMin, errMax);
    qHM->SetXTitle("Charge");
    qHM->SetYTitle("Error");

    int n;
    float *amps;
    TH1F **templates = Utils::getTemplates(new TFile("norms6.root"), n, amps);
    for (int i = 0; i < n; i++)
        templates[i] = Utils::zeroAlignAndNormalizeInPlace(
            Utils::extractTemplatePeak(templates[i]));

    Consumer<TObjectPair<TH1F, TGraph>, Result> *finderS =
        new SimpleCurveFinder(templates, n);
    Consumer<TObjectPair<TH1F, TGraph>, Result> *finderR =
        new RootCurveFinder(templates, amps, n);
    Consumer<TObjectPair<TH1F, TGraph>, Result> *finderI =
        new IntegralCurveFinder();
    Consumer<TObjectPair<TH1F, TGraph>, Result> *finderM =
        new MeanCurveFinder(templates, amps, n);

    // MeanCurveFinder *meanOrig = (MeanCurveFinder *)finderM;

    Consumer<Result, Result> *histoS1 = new Histogram2Accumulator(
        ampHS, Histogram2Accumulator::HistogramType::AMP);
    Consumer<Result, Result> *histoS2 =
        new Histogram2Accumulator(qHS, Histogram2Accumulator::HistogramType::Q);

    Consumer<Result, Result> *histoR1 = new Histogram2Accumulator(
        ampHR, Histogram2Accumulator::HistogramType::AMP);
    Consumer<Result, Result> *histoR2 =
        new Histogram2Accumulator(qHR, Histogram2Accumulator::HistogramType::Q);

    Consumer<Result, Result> *histoI1 = new Histogram2Accumulator(
        ampHI, Histogram2Accumulator::HistogramType::AMP);
    Consumer<Result, Result> *histoI2 =
        new Histogram2Accumulator(qHI, Histogram2Accumulator::HistogramType::Q);

    Consumer<Result, Result> *histoM1 = new Histogram2Accumulator(
        ampHM, Histogram2Accumulator::HistogramType::AMP);
    Consumer<Result, Result> *histoM2 =
        new Histogram2Accumulator(qHM, Histogram2Accumulator::HistogramType::Q);

    Consumer<Result, Result> *histoS = histoS2->after(histoS1);
    Consumer<Result, Result> *histoR = histoR2->after(histoR1);
    Consumer<Result, Result> *histoI = histoI2->after(histoI1);
    Consumer<Result, Result> *histoM = histoM2->after(histoM1);

    finderS = histoS->after(finderS);
    finderR = histoR->after(finderR);
    finderI = histoI->after(finderI);
    finderM = histoM->after(finderM);

    SplitterConsumer<TObjectPair<TH1F, TGraph>, Result> *finders =
        finderS->nextTo(finderR, false)->with(finderI)->with(finderM);

    finders->setCleanup(true);

    TFile *out = new TFile(Form("res-%.0f.root", freq), "RECREATE");
    Consumer<TObjectPair<TH1F, TGraph>, TObjectPair<TH1F, TGraph>> *fAcc =
        new TFileAccumulator<TH1F, TGraph>(out);

    auto lambda = [](TObjectPair<TH1F, TGraph> *p) {
        p->e1->SetLineColor(kBlue);

        p->e2->SetLineColor(kWhite);
        p->e2->SetMarkerColor(kRed);

        return p;
    };

    Consumer<TObjectPair<TH1F, TGraph>, TObjectPair<TH1F, TGraph>> *color =
        new LambdaConsumer<TObjectPair<TH1F, TGraph>,
                           TObjectPair<TH1F, TGraph>>(lambda);

    fAcc = fAcc->after(color);

    Consumer<TObjectPair<TH1F, TGraph>, list<Result *>> *pipeline =
        finders->after(fAcc);

    Task<TObjectPair<TH1F, TGraph>, list<Result *>> *task = pipeline->asTask();
    TaskRunner<TObjectPair<TH1F, TGraph>, list<Result *>> *runner =
        task->with(provider, true);

    printf("Running\n");
    runner->runVoid();

    TFile *histsOut =
        new TFile(Form("out/histsOut-%.0f.root", freq), "RECREATE");
    histsOut->cd();

    // ampHM->GetYaxis()->SetLimits(-100, -300);
    // qHM->GetYaxis()->SetLimits(-100, -300);

    TH1D *hM, *hS;
    TCanvas *cSlice = new TCanvas();
    TCanvas *c = new TCanvas("histsS", "Simple method", 1200, 1000);
    c->Divide(2);

    c->cd(1);
    ampHS->Draw();
    ampHS->Write();

    c->cd(2);
    qHS->Draw();
    qHS->Write();
    c->SaveAs(Form("out/th2f/histsS-%.0f.png", freq));
    c->Print(Form("out/hists-%.0f.pdf(", freq));

    TCanvas *c2 = new TCanvas("histsR", "Fit method", 1200, 1000);
    c2->Divide(2);

    c2->cd(1);
    ampHR->Draw();
    ampHR->Write();

    c2->cd(2);
    qHR->Draw();
    qHR->Write();
    c2->SaveAs(Form("out/th2f/histsR-%.0f.png", freq));
    c2->Print(Form("out/hists-%.0f.pdf", freq));

    TCanvas *c3 = new TCanvas("histsI", "Integral method", 1200, 1000);
    c3->Divide(2);

    c3->cd(1);
    ampHI->Draw();
    ampHI->Write();

    c3->cd(2);
    qHI->Draw();
    qHI->Write();
    c3->SaveAs(Form("out/th2f/histsI-%.0f.png", freq));
    c3->Print(Form("out/hists-%.0f.pdf", freq));

    TCanvas *c4 = new TCanvas("histsM", "Mean method", 1200, 1000);
    c4->Divide(2);

    c4->cd(1);
    ampHM->Draw();
    ampHM->Write();

    c4->cd(2);
    qHM->Draw();
    qHM->Write();
    c4->SaveAs(Form("out/th2f/histsM-%.0f.png", freq));
    c4->Print(Form("out/hists-%.0f.pdf", freq));

    TH1 *h1, *h2, *h3, *h4;
    TCanvas *c5 = new TCanvas("histsSy", "Simple method", 1200, 1000);
    h1 = ampHS->ProjectionY("hSy");
    h1->Draw();
    h1->Write();
    c5->SaveAs(Form("out/th2f/hSy-%.0f.png", freq));
    c5->Print(Form("out/hists-%.0f.pdf", freq));

    // h1->Delete();

    TCanvas *c6 = new TCanvas("histsRy", "Fit method", 1200, 1000);
    h2 = ampHR->ProjectionY("hRy");
    h2->Draw();
    h2->Write();
    c6->SaveAs(Form("out/th2f/hRy-%.0f.png", freq));
    c6->Print(Form("out/hists-%.0f.pdf", freq));

    // h->Delete();

    TCanvas *c7 = new TCanvas("histsIy", "Integral method", 1200, 1000);
    h3 = ampHI->ProjectionY("hIy");
    h3->Draw();
    h3->Write();
    c7->SaveAs(Form("out/th2f/hIy-%.0f.png", freq));
    c7->Print(Form("out/hists-%.0f.pdf", freq));

    TCanvas *c8 = new TCanvas("histsMy", "Mean method", 1200, 1000);
    h4 = ampHM->ProjectionY("hMy");
    h4->Draw();
    h4->Write();
    c8->SaveAs(Form("out/th2f/hMy-%.0f.png", freq));
    c8->Print(Form("out/hists-%.0f.pdf)", freq));

    Utils::fitSlicesY(ampHS, hM, hS);
    cSlice->cd(1);
    hM->Draw("HIST");
    hM->Print();

    auto *tmpF = gFile;
    new TFile("out/slices/hists.root", "recreate");
    hM->Write();
    gFile->Close();
    gFile = tmpF;

    hM->SaveAs("out/slices/ampHS-mean.C");
    cSlice->SaveAs(Form("out/slices/%s.png", hM->GetName()));
    cSlice->Clear();
    hM->Delete();
    cSlice->cd(1);
    hS->Draw("HIST");
    cSlice->SaveAs(Form("out/slices/%s.png", hS->GetName()));
    cSlice->Clear();
    hS->Delete();

    Utils::fitSlicesY(ampHR, hM, hS);
    cSlice->cd(1);
    hM->Draw("HIST");
    cSlice->SaveAs(Form("out/slices/%s.png", hM->GetName()));
    cSlice->Clear();
    hM->Delete();
    cSlice->cd(1);
    hS->Draw("HIST");
    cSlice->SaveAs(Form("out/slices/%s.png", hS->GetName()));
    cSlice->Clear();
    hS->Delete();

    Utils::fitSlicesY(ampHI, hM, hS);
    cSlice->cd(1);
    hM->Draw("HIST");
    cSlice->SaveAs(Form("out/slices/%s.png", hM->GetName()));
    cSlice->Clear();
    hM->Delete();
    cSlice->cd(1);
    hS->Draw("HIST");
    cSlice->SaveAs(Form("out/slices/%s.png", hS->GetName()));
    cSlice->Clear();
    hS->Delete();

    Utils::fitSlicesY(ampHM, hM, hS);
    cSlice->cd(1);
    hM->Draw("HIST");
    cSlice->SaveAs(Form("out/slices/%s.png", hM->GetName()));
    cSlice->Clear();
    hM->Delete();
    cSlice->cd(1);
    hS->Draw("HIST");
    cSlice->SaveAs(Form("out/slices/%s.png", hS->GetName()));
    cSlice->Clear();
    hS->Delete();

    // meanOrig->saveStat();
    pipeline->finalize();
    out->Close();
}