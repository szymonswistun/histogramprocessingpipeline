#pragma once

#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"

#include "TF1.h"
#include "TF2.h"

#include "TGraph.h"

#include "TCanvas.h"
#include "TFile.h"
#include "TThread.h"