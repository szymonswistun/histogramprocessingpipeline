#pragma once

#include <cstring>
#include <list>

#include "Abstracts.hpp"

using namespace std;

template <typename T>
class TObjectProvider : public Provider<T> {
   public:
    void add(const char *name) {
        char *cpy = new char[strlen(name) + 1];
        fNames.push_back(strcpy(cpy, name));
    }

    void add(const char **names = nullptr, int n = 0) {
        if (names) {
            for (int i = 0; i < n; i++) {
                add(names[i]);
            }
        }
    }

    void add(list<const char *> *fL) {
        for (auto *n : *fL) {
            // printf("Adding %s\n", n);
            add(n);
        }
    }

    TObjectProvider(list<const char *> *fL) { add(fL); }

    TObjectProvider(const char **names = nullptr, int n = 0) { add(names, n); }

    TObjectProvider(const char *f) { fNames.push_back(f); }

    bool hasMore() override { return curFile || !fNames.empty(); }

   private:
    list<const char *> fNames;
    TFile *curFile = nullptr;
    TList *curList = nullptr;
    TIter *fIter = nullptr;

    int inFileI = 0;
    long globalI = 0;

    bool setNextFile() {
        if (fNames.empty())
            return false;
        else {
            curFile = new TFile(fNames.front());
            fNames.pop_front();
            curList = curFile->GetListOfKeys();
            fIter = new TIter(curList);
            return true;
        }
    }

    T *getInt(bool threadSafe = false) override {
        if (!hasMore()) return nullptr;
        // printf("Start\n");

        if (!fIter || !curFile || !curList) {
            bool res = setNextFile();
            if (!res) return nullptr;
        }

        TObject *key = (*fIter)();
        // printf("Key fetched\n");
        if (key) {
            const char *oName = key->GetName();
            TObject *obj = curFile->Get(oName);
            ((T *)obj)->SetName(Form("%s-%s", oName, curFile->GetName()));

            globalI++;
            return (T *)obj;
            // printf("Object fetched\n");

        } else {
            // printf("Invalidating file\n");
            // if (curFile) {
            //     curFile->Close();
            //     delete curFile;
            // }
            // if (curList) delete curList;

            if (curFile) curFile->Close();
            curFile = nullptr;
            curList = nullptr;
            fIter = nullptr;
            return getInt(threadSafe);
        }
        // delete oName;

        // printf("%s\n", obj->GetName());
    }
};