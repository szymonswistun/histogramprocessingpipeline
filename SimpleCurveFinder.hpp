#pragma once

#include "Abstracts.hpp"
#include "Common.hpp"

#define TEMPLATE 0
#define COUNT 2

class SimpleCurveFinder : public Consumer<TObjectPair<TH1F, TGraph>, Result> {
   private:
    TH1F **templates;
    int nTemplates;
    TFile *fitOut;
    int fI = 0;
    int count = 0;
    TH2F *peakErr;
    TH2F *peakOldErr;
    TH2F *histPeakQErr;

    static void interpolatePeak(double &x, double &y, double x0, double y0,
                                double x1, double y1, double x2, double y2,
                                double x3, double y3) {
        double a0 = (y1 - y0) / (x1 - x0);
        double b0 = y0 - a0 * x0;
        double a1 = (y3 - y2) / (x3 - x2);
        double b1 = y2 - a1 * x2;

        x = (b1 - b0) / (a0 - a1);
        y = a1 * x + b1;
    }

    static void interpolatePeak(double &x, double &y, double *xArr,
                                double *yArr) {
        SimpleCurveFinder::interpolatePeak(x, y, xArr[0], yArr[0], xArr[1],
                                           yArr[1], xArr[3], yArr[3], xArr[2],
                                           yArr[2]);
    }

    // g2 = new TGraph; iG2 = 0; for(int i = 0; i < g->GetN() && iG2 <= 4; i++)
    // { g->GetPoint(i, x, y); if (y>60) g2->SetPoint(iG2++, x, y); }

    static Result *SimpleAlgorithm(TH1F *h, TGraph *g, TH1F **templates,
                                   TFile *fitOut, int &fI, int &count,
                                   TH2F *peakErr, TH2F *peakOldErr,
                                   TH2F *histPeakQErr) {
        TH1F *t = templates[TEMPLATE];

        double x, y, xStart = -1, xMax, yMax = -1000;
        RotaryBuffer<Point> buf(COUNT);
        TGraph *g2 = nullptr;
        int g2i = 0;

        for (int i = 0; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);
            if (g2) {
                if (g2i >= COUNT * 2) break;

                g2->SetPoint(g2i++, x, y);
            } else {
                if (y > yMax) {
                    xMax = x;
                    yMax = y;
                    buf.push(new Point(x, y));
                } else {
                    buf.setToOldest();
                    g2 = new TGraph();
                    while (g2i < COUNT) {
                        Point *p = buf.pop();
                        if (xStart == -1) xStart = p->x;
                        g2->SetPoint(g2i++, p->x, p->y);
                    }
                    // g2->SetPoint(g2i++, x, y);
                }
            }
        }

        g2->GetPoint(g2->GetN() - 1, x, y);
        double xEnd = x - xStart;

        TF1 *f =
            new TF1("g", "[0] * (x + [3])^2 + [1] * (x + [3]) + [2]", -20, 20);

        g2->Fit(f, "Q", "");
        f = g2->GetFunction("g");
        double yMaxOld = yMax;
        yMax = f->GetMaximum() * 0.95;
        fitOut->WriteTObject(g2, Form("g%i", fI++));
        peakErr->Fill(h->GetMaximum(), h->GetMaximum() - yMax);
        peakOldErr->Fill(h->GetMaximum(), h->GetMaximum() - yMaxOld);
        histPeakQErr->Fill(h->GetMaximum(),
                           (h->Integral() - t->Integral() * h->GetMaximum()) /
                               h->Integral() * 100);

        // fitOut->WriteTObject(peakErr, "peakErr");
        // fitOut->WriteTObject(histPeakQErr, "histPeakQErr");

        fitOut->cd();
        peakErr->Write();
        peakOldErr->Write();
        histPeakQErr->Write();

        if (yMaxOld > 20 && abs(yMax - yMaxOld) > 100) yMax = yMaxOld;

        // if (abs(yMax - yMaxOld) > 20) {
        //     printf("id: %i - yMax=%lf, yMaxOld=%lf - g2N=%i\n", fI, yMax,
        //     yMaxOld, g2->GetN());
        // }

        Result *res = new Result();
        res->graph = t->Integral() * yMax;
        res->orig = h->Integral();
        res->err = (res->orig - res->graph) / res->orig * 100;
        res->amp = yMax;

        res->type = FinderType::Simple;

        return res;
    }

    enum State { RISE, FALL };

    static Result *SimpleAlgorithm2(TH1F *h, TGraph *g, TH1F **templates,
                                    TFile *fitOut, int &fI, int &count,
                                    TH2F *peakErr, TH2F *peakOldErr,
                                    TH2F *histPeakQErr) {
        TH1F *t = templates[TEMPLATE];

        double x, y, xStart = -1, xMax, yMax = -1000;
        double xPrev, yPrev;
        double xArr[4], yArr[4];
        int arrI = 0;

        g->GetPoint(0, x, y);
        State state = RISE;

        for (int i = 1; i < g->GetN(); i++) {
            xPrev = x;
            yPrev = y;
            g->GetPoint(i, x, y);

            switch (state) {
                case RISE:
                    if (abs(yPrev - y) < 30) {
                        xArr[0] = xPrev;
                        xArr[1] = x;

                        yArr[0] = yPrev;
                        yArr[1] = y;

                        arrI = 2;
                        state = FALL;
                    }
                    break;
                case FALL:
                    xArr[arrI] = x;
                    yArr[arrI++] = y;
            }

            if (arrI >= 4) {
                SimpleCurveFinder::interpolatePeak(xMax, yMax, xArr, yArr);
                break;
            }
        }

        printf("xMax=%lf, yMax=%lf\n", xMax, yMax);

        // yMax = f->GetMaximum();
        peakErr->Fill(h->GetMaximum(), h->GetMaximum() - yMax);
        histPeakQErr->Fill(h->GetMaximum(),
                           (h->Integral() - t->Integral() * h->GetMaximum()) /
                               h->Integral() * 100);

        // fitOut->WriteTObject(peakErr, "peakErr");
        // fitOut->WriteTObject(histPeakQErr, "histPeakQErr");

        fitOut->cd();
        peakErr->Write();
        histPeakQErr->Write();

        // if (yMaxOld > 20 && abs(yMax - yMaxOld) > 100) yMax = yMaxOld;

        // if (abs(yMax - yMaxOld) > 20) {
        //     printf("id: %i - yMax=%lf, yMaxOld=%lf - g2N=%i\n", fI, yMax,
        //     yMaxOld, g2->GetN());
        // }

        Result *res = new Result();
        res->graph = t->Integral() * yMax;
        res->orig = h->Integral();
        res->err = (res->orig - res->graph) / res->orig * 100;
        res->amp = yMax;

        res->type = FinderType::Simple;

        return res;
    }

    static Result *SimpleAlgorithm3(TH1F *h, TGraph *g, TH1F **templates,
                                    TFile *fitOut, int &fI, int &count,
                                    TH2F *peakErr, TH2F *peakOldErr,
                                    TH2F *histPeakQErr) {
        TH1F *t = templates[TEMPLATE];
        double params[] = {-0.00000036, 0.00009405, 0.00000000, 0.00000000, 50};

        double x, y, xMax, yMax = -1000;

        for (int i = 0; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);

            if (y > yMax) {
                xMax = x;
                yMax = y;
            }
        }

        double yMaxOld = yMax;
        // yMax += 5;
        // yMax =
        //     yMaxOld * (yMaxOld * (yMaxOld * (yMaxOld * params[0] + params[1])
        //     +
        //                           params[2]) +
        //                params[3]) +
        //     params[4];
        // yMax *= 1.1 + (yMaxOld / 8000);

        Result *res = new Result();
        res->graph = t->Integral() * yMax;
        res->orig = h->Integral();
        res->err = (res->orig - res->graph) / res->orig * 100;
        res->amp = h->GetMaximum();

        res->type = FinderType::Simple;

        return res;
    }

    Result *consumeInt(TObjectPair<TH1F, TGraph> *p,
                       bool threadSafe = false) override {
        // printf("SimpleCurveFinder\n");
        Result *ret = nullptr;
        if (p && p->e1 && p->e2) {
            ret = SimpleAlgorithm3(p->e1, p->e2, templates, fitOut, fI, count,
                                   peakErr, peakOldErr, histPeakQErr);
            // ret = SimpleAlgorithm2(p->e1, p->e2, templates, fitOut, fI,
            // count, peakErr, peakOldErr, histPeakQErr); p->e1->Delete();
            // p->e2->Delete();
            // delete p->e1, p->e2;
            // delete p;
        }

        return ret;
    }

   public:
    SimpleCurveFinder(TH1F **templates, int n) {
        fitOut = new TFile("fit.root", "RECREATE");
        peakErr = new TH2F("peakErr", "", 400, 20, 200, 400, -100, 100);
        peakOldErr = new TH2F("peakOldErr", "", 400, 20, 200, 400, -100, 100);
        histPeakQErr = new TH2F("histPeakQErr", "", 400, 20, 200, 400, -50, 50);

        this->templates = templates;
        this->nTemplates = n;
    }
};