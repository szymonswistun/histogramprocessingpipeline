#include "Pipeline.hpp"
#include "ReportFormatter.hpp"

#define FILE_COUNT 7

using namespace std;

void Test() {
    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < FILE_COUNT; i++)
        names->push_back(Form("data/signals%i.root", i));
    Provider<TH1F> *provider = new TObjectProvider<TH1F>(names);

    TH1F *peaks1 = new TH1F("peaks1", "", 100, 0, 300);
    TH1F *peaks2 = new TH1F("peaks2", "", 100, 0, 100);
    TH1F *thres = new TH1F("thres", "", 100, 0, 200);

    auto lambda = [&](TH1F *h) {
        Utils::extractPeakInPlace(h);
        int peak = h->GetMaximumBin();
        // printf("%s\n", h->GetName());
        peaks1->Fill(peak);
        peaks2->Fill(peak);
        float thr = h->GetBinContent(peak - 25);
        thres->Fill(thr);
        return h;
    };
    Consumer<TH1F, TH1F> *cons = new LambdaConsumer<TH1F, TH1F>(lambda);
    auto *task = cons->asTask();
    auto *runner = task->with(provider);
    runner->runVoid();

    auto *c = new TCanvas("peaksC", "peaks", 1800, 600);
    c->Divide(3);
    c->cd(1);
    peaks1->Draw();
    c->cd(2);
    peaks2->Draw();
    c->cd(3);
    thres->Draw();
}