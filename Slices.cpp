#include "Utils.hpp"

void Slices() {
    TFile* f = new TFile("out/histsOut-20.root");
    TH2F* h = (TH2F*)f->Get("ampHI");

    auto* arr = Utils::fitSlicesY(h);

    for (auto* e : *arr) {
        auto* c = new TCanvas();
        e->Draw();
    }
}