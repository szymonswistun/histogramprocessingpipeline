#include "IntegralCurveFinder.hpp"
#include "Pipeline.hpp"
#include "ReportFormatter.hpp"

#define FILE_COUNT 7

ReportFormatter *IntegralTest(int i = 0, float freq = 20) {
    int n;
    float *amps;
    TH1F **templates = Utils::getTemplates(new TFile("norms6.root"), n, amps);
    for (int i = 0; i < n; i++)
        templates[i] = Utils::zeroAlignAndNormalizeInPlace(
            Utils::extractTemplatePeak(templates[i]));

    Consumer<TObjectPair<TH1F, TGraph>, Result> *test =
        new IntegralCurveFinder();

    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < FILE_COUNT; i++)
        names->push_back(Form("data/signals%i.root", i));
    Provider<TObjectPair<TH1F, TGraph>> *provider = new Preprocessor(
        new Interpolator(freq, new TObjectProvider<TH1F>(names)));

    ReportFormatter *formatterB =
        new ReportFormatter("", 'I', 20, 20, 200, 4000, 30000, 100, -150, 150);

    ReportFormatter *formatterN =
        new ReportFormatter("", 'J', 20, 20, 200, 4000, 30000, 20, -10, 10);

    test = test->before(formatterB);
    test = test->before(formatterN);

    auto lambdaP = [](Result *r) {
        if (r) printf("Amp: %.2f, Err: %.5f\n", r->amp, r->err);
        return r;
    };

    Consumer<Result, Result> *lambdaPC =
        new LambdaConsumer<Result, Result>(lambdaP);
    // test = test->before(lambdaPC);

    int cnt = 0;
    auto lambdaS = [&](Result *r) {
        printf("\r%i/%i - %lf%%", ++cnt, FILE_COUNT * 1000, r->err);
        return r;
    };
    Consumer<Result, Result> *lambdaSC =
        new LambdaConsumer<Result, Result>(lambdaS);
    test = test->before(lambdaSC);

    auto *runner = test->asTask()->with(provider);
    runner->runVoid();

    test->finalize();
    return formatterB;
}

#ifdef __GNUC__
int main(int argc, char **argv) {
    TApplication App(argv[0], &argc, argv);
    IntegralTest();
    return 0;
}
#endif
