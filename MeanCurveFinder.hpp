#pragma once

#include "Abstracts.hpp"
#include "Common.hpp"
#include "TDD2FTab.hpp"
#include "Utils.hpp"

#define TEMPLATE 0
#define COUNT 2

class MeanCurveFinder : public Consumer<TObjectPair<TH1F, TGraph>, Result> {
   private:
    TH1F **templates;
    int nTemplates;

    int i = 0;

    float *amps;

    Result *meanAlgorithm(TH1F *h, TGraph *g, bool verbosity = false) {
        float amp = h->GetMaximum();
        TH1F *t = templates[getClosest(amps, nTemplates, amp)];
        t = Utils::extractTemplatePeak(t);

        float hI = h->Integral();
        float tI = t->Integral();
        float scale = Utils::getMean(g) / Utils::getMean(t);
        float resI = tI * scale;
        float errS = (hI - resI) / hI * 100;

        Result *res = new Result();
        res->graph = resI;
        res->orig = h->Integral();
        res->err = errS;
        res->amp = h->GetMaximum();

        g->SetMarkerColor(kRed);
        g->SetLineColor(kRed);

        res->type = FinderType::Mean;

        g->Delete();
        h->Delete();

        return res;
    }

    Result *consumeInt(TObjectPair<TH1F, TGraph> *p,
                       bool threadSafe = false) override {
        Result *ret = nullptr;

        if (p) {
            ret = meanAlgorithm(p->e1, p->e2);
        }

        return ret;
    }

    int getClosest(float *arr, int n, float v) {
        int i;
        for (int i = 1; i < n; i++) {
            if (arr[i] > v) return i - 1;
        }
        return n - 1;
    }

   public:
    MeanCurveFinder(TH1F **templates, float *amps, int n) {
        this->templates = Utils::getTemplates(new TFile("norms6.root"),
                                              this->nTemplates, this->amps);

        this->amps = amps;
        this->nTemplates = n;
    }
};