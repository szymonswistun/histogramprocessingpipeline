#pragma once

#include "Abstracts.hpp"

template <typename T1, typename T2>
class TFileAccumulator : public Accumulator<TObjectPair<T1, T2>, TFile> {
   public:
    TFileAccumulator(TFile* out, const char* d1 = "hist",
                     const char* d2 = "graph")
        : Accumulator<TObjectPair<T1, T2>, TFile>(out) {
        dir1 = out->mkdir(Form("%ss", d1));
        dir2 = out->mkdir(Form("%ss", d2));
        this->d1 = d1;
        this->d2 = d2;
    }

    template <typename T3>
    TerminalConsumer<TObjectPair<T1, T2>, TFile, T3>* toTerminalConsumer() {
        return new TerminalConsumer<TObjectPair<T1, T2>, TFile, T3>(this);
    }

    virtual void finalize() override { this->acc->Close(); }

   private:
    TDirectory *dir1, *dir2;
    const char *d1, *d2;
    int count = 0;

    void accumulate(TObjectPair<T1, T2>* e, bool threadSafe = false) override {
        if (!e) return;

        T1* e1 = e->e1;
        T2* e2 = e->e2;

        const char *name1 = Form("signal%i", count),
                   *name2 = Form("signal%i", count);

        e1->SetName(name1);
        e2->SetName(name2);

        dir1->WriteTObject(e1, name1);
        dir2->WriteTObject(e2, name2);
        count++;
    }
};