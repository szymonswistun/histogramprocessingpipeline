#include "Pipeline.hpp"

using namespace std;

void PeakAnalysis() {
    list<const char*>* names = new list<const char*>();
    for (int i = 0; i < 7; i++)
        names->push_back(Form("data/signals%i.root", i));

    auto* provider = new Preprocessor(
        new Interpolator(20, new TObjectProvider<TH1F>(names)));

    TH1F* amps = new TH1F("amps", "", 600, 0, 300);
    HistogramAccumulator* hAmps = new HistogramAccumulator(amps);

    TH1F* peaks = new TH1F("peaks", "", 600, 0, 300);
    HistogramAccumulator* hPeaks = new HistogramAccumulator(peaks);

    // function<TObjectPair<TH1F, TGraph>(Pair<float, float>)
    auto peakExtract = [&](TObjectPair<TH1F, TGraph>* p) {
        double xMax, yMax;

        Utils::getMaxPointAdjusted(xMax, yMax, p->e2);
        return new Pair<double, double>(&xMax, &yMax);
    };

    Consumer<TObjectPair<TH1F, TGraph>, Pair<double, double>>* extractor =
        new LambdaConsumer<TObjectPair<TH1F, TGraph>, Pair<double, double>>(
            peakExtract);

    auto* pairSplitter =
        new PairSplitterConsumer<double, double, double, double>(hAmps, hPeaks,
                                                                 true);

    auto* pipeline = extractor->before(pairSplitter);
    auto* task = pipeline->asTask();
    auto* runner = task->with(provider);

    runner->runVoid();

    TCanvas* c1 = new TCanvas("c1", "", 1200, 600);
    c1->Divide(2);
    c1->cd(1);
    peaks->Draw();

    c1->cd(2);
    amps->Draw();

    c1->SaveAs("peaks.png");
}