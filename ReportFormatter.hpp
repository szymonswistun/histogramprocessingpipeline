#pragma once

#include "Pipeline.hpp"

class ReportFormatter : public Accumulator<Result, TCanvas> {
   public:
    ReportFormatter(const char* text, char letter, int nBinX, double xLowAmp,
                    double xUpAmp, double xLowQ, double xUpQ, int nBinY,
                    double yLow, double yUp, TCanvas* can = nullptr)
        : Accumulator<Result, TCanvas>(can) {
        this->text = text;
        this->letter = letter;

        this->amp = new TH2F(Form("amp%c", letter), "", nBinX, xLowAmp, xUpAmp,
                             nBinY, yLow, yUp);
        this->amp->SetXTitle("Amplitude");
        this->amp->SetYTitle("Error %");

        this->q = new TH2F(Form("q%c", letter), "", nBinX, xLowQ, xUpQ, nBinY,
                           yLow, yUp);
        this->q->SetXTitle("Charge");
        this->q->SetYTitle("Error %");
    }

    void finalize() override {
        if (!this->acc)
            this->acc = new TCanvas(Form("report%c", letter),
                                    Form("report%c", letter), 1200, 1200);

        TCanvas* c = this->acc;

        c->Divide(2, 3);
        gStyle->SetOptFit(1111);
        c->cd(1);
        gPad->SetGrid(0, 1);
        amp->Draw("colz");
        c->cd(2);
        gPad->SetGrid(0, 1);
        q->Draw("colz");
        amp->SaveAs(Form("ampH%c.C", letter));
        TH1D *hMa, *hSa, *hMq, *hSq;
        Utils::fitSlicesY(amp, hMa, hSa);
        hMa->SetXTitle("Amplitude");
        hMa->SetYTitle("Error %");
        hSa->SetXTitle("Amplitude");
        hSa->SetYTitle("Error %");

        Utils::fitSlicesY(q, hMq, hSq);
        hMq->SetXTitle("Charge");
        hMq->SetYTitle("Error %");
        hSq->SetXTitle("Charge");
        hSq->SetYTitle("Error %");

        c->cd(3);
        auto* a = amp->GetYaxis();
        gPad->SetGrid(0, 1);
        hMa->GetYaxis()->SetRangeUser(hMa->GetMinimum() * 1.1,
                                      hMa->GetMaximum() * 1.1);
        hMa->Fit("pol0");
        hMa->Draw();
        c->cd(4);
        a = q->GetYaxis();
        gPad->SetGrid(0, 1);
        hMq->GetYaxis()->SetRangeUser(hMq->GetMinimum() * 1.1,
                                      hMq->GetMaximum() * 1.1);
        hMa->Fit("pol0");
        hMq->Draw();

        c->cd(5);
        a = amp->GetYaxis();
        hSa->GetYaxis()->SetRangeUser(0, hSa->GetMaximum() * 1.1);
        hSa->Draw();
        c->cd(6);
        a = q->GetYaxis();
        hSq->GetYaxis()->SetRangeUser(0, hSq->GetMaximum() * 1.1);
        hSq->Draw();
        img = Form("out/report%c.png", letter);
        c->SaveAs(img);
    }

    const char* getImg() { return img; }

   private:
    TCanvas* can;
    const char* text;
    TH2F *amp, *q;
    char letter;
    const char* img;
    // int cnt = 0, all = 0;

    void accumulate(Result* res, bool threadSafe = false) override {
        if (res) {
            amp->Fill(res->amp, res->err);
            q->Fill(res->orig, res->err);
            // printf("\r%i/%i - %s", cnt++, all, amp->GetName());
        }
        // all++;
    }
};