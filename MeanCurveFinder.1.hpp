#pragma once

#include "Abstracts.hpp"
#include "Common.hpp"
#include "TDD2FTab.hpp"
#include "Utils.hpp"

#define TEMPLATE 0
#define COUNT 2

class MeanCurveFinder : public Consumer<TObjectPair<TH1F, TGraph>, Result> {
   private:
    // TGraph **templates;
    TH1F **templates;
    int nTemplates;
    TDD2FTab *interpolator;

    TFile *out;
    TH1F *stat1, *stat2;
    int i = 0;
    static int fI;

    float *amps;

    Result *meanAlgorithm(TH1F *h, TGraph *g) {
        // TGraph *t = templates[0];
        float amp = g->GetMaximum();
        TH1F *t = templates[getClosest(amps, nTemplates, amp)];

        // double mult = .76572;
        // double mult = .5;
        double mult = 1;
        double tMean = Utils::getMean(t), gMean = Utils::getMean(g);
        // printf("tMean: %lf, gMean: %lf\n", tMean, gMean);
        double scale = gMean / tMean * mult;
        // double scale = gMean / tMean * mult;
        // stat2->SetBinContent(i, scale);
        stat1->SetBinContent(i,
                             (h->GetMaximum() - scale) / h->GetMaximum() * 100);
        stat2->SetBinContent(i++, h->GetMaximum() - scale);

        TDirectory *cur = out->mkdir(Form("sig%i", fI++));
        cur->WriteTObject(h, "hist");
        cur->WriteTObject(g, "graph");

        Result *res = new Result();
        res->graph = t->Integral() * scale;
        res->orig = h->Integral();
        res->err = (res->orig - res->graph) / res->orig * 100;
        res->amp = h->GetMaximum();

        res->type = FinderType::Mean;

        return res;
    }

    Result *consumeInt(TObjectPair<TH1F, TGraph> *p,
                       bool threadSafe = false) override {
        Result *ret = nullptr;

        if (p) {
            ret = meanAlgorithm(p->e1, p->e2);
        }

        return ret;
    }

    int getClosest(float *arr, int n, float v) {
        int i;
        for (int i = 1; i < n; i++) {
            if (arr[i] > v) return i - 1;
        }
        return n - 1;
    }

   public:
    MeanCurveFinder(float freq, TH1F **templates, int n) {
        this->interpolator = new TDD2FTab(freq, 1, 3);

        // this->templates = new TGraph *[n];
        // for (int i = 0; i < n; i++) {
        //     this->templates[i] = this->interpolator->convert(templates[i]);
        // }
        this->templates = Utils::getTemplates(new TFile("norms6.root"),
                                              this->nTemplates, this->amps);

        this->out = new TFile("out/meanOut.root", "RECREATE");

        this->nTemplates = n;
        this->stat1 = new TH1F("template", "", 3430, 0, 3430);
        this->stat1->SetLineColor(kBlue);
        this->stat2 = new TH1F("interpolation", "", 3430, 0, 3430);
        this->stat2->SetLineColor(kRed);
    }

    void finalize() override {
        TCanvas *c = new TCanvas("cStat", "", 1200, 600);
        c->Divide(2);
        c->cd(1);
        this->stat1->Draw();
        c->cd(2);
        this->stat2->Draw();
        c->SaveAs("out/stat.png");

        this->out->cd();
        stat1->Write();
        stat2->Write();
        this->out->Close();
    }
};

int MeanCurveFinder::fI = 0;