#pragma once

#include "Types.h"

#include <cstdio>
#include <functional>

#include "TApplication.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TSystem.h"