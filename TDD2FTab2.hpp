#pragma once

class TDD2FTab2 {
   public:
    TDD2FTab2(Float_t freq = 50000000000, Float_t scale = 0.000000001,
              float amp = 200) {
        this->sw = CreateSineWave(freq, scale, amp);
    }

    void SetFrequency(Float_t freq, Float_t scale = 0.000000001,
                      Float_t amp = 100) {
        ChangeFrequency(freq, scale, amp);
    }

    void ChangeFrequency(Float_t freq, Float_t scale = 0.000000001,
                         Float_t amp = 100) {
        if (this->sw != NULL) delete this->sw;

        this->sw = CreateSineWave(freq, scale, amp);
    }

    TGraph* convert(TH1F* hist, TGraph* g = new TGraph(), float threshold = 95,
                    float delay = 10, int approx_steps = 10 ^ (3)) {
        return Intersections(hist, this->sw, g, threshold, delay, approx_steps);
    }

    ~TDD2FTab2() { delete sw; }

    const int DELAY = 10;

   private:
    TF1* sw;

    static Float_t CalculateFrequencyParameter(Float_t freq,
                                               Float_t scale = 0.000000001) {
        const Float_t f50hz = 0.1 / (TMath::Pi() / 9.875);
        return f50hz * (freq * scale / 50);
    }

    static TF1* CreateSineWave(Float_t freq, Float_t scale = 0.000000001,
                               Float_t amp = 165, int npx = 8192) {
        TF1* sine = new TF1("sine", "[0] * sin([1] * x + [2]) + [3]", -1, 1025);
        sine->SetParameters(amp / 2, CalculateFrequencyParameter(freq, scale),
                            0, amp / 2);
        sine->SetNpx(npx);
        return sine;
    }

    static TGraph* Intersections(TH1F* h1, TF1* sw, TGraph* g = new TGraph(),
                                 float threshold = 95, float delay = 10,
                                 int approx_steps = 10 ^ (3)) {
        int i, n = h1->GetSize();

        TGraph* h_g1 = new TGraph(h1);

        int hStart = delay;
        for (int i = 0; h1->GetBinContent(i) < threshold; i++) hStart++;

        // printf("Orig: %d|%f, Start: %d|%f\n", hStart - 10,
        //        h1->GetBinContent(hStart - 10), hStart,
        //        h1->GetBinContent(hStart));
        int g_i = 0;

        g->SetMarkerColor(kRed);
        Float_t sign = h1->GetBinContent(hStart) - sw->Eval(0);
        for (i = 1; i < n && i < 300; i++) {
            Float_t diff = h1->GetBinContent(hStart + i) - sw->Eval(i);

            if (sign * diff <= 0) {
                Float_t cur = sign;
                Float_t step_size = 1 / approx_steps;
                // Float_t h1_step_size = (h1->GetBinContent(i) -
                // h1->GetBinContent(i - 1)) / approx_steps;
                sign = diff;
                Float_t cur_x = hStart + i - 1;

                int j;

                for (j = 1; j < approx_steps; j++) {
                    Float_t tmp = cur;
                    cur_x += step_size;
                    cur = sw->Eval(cur_x + (0.02 * gRandom->Gaus(0, 0.3))) -
                          h_g1->Eval(cur_x);
                    if (abs(tmp) < abs(cur)) {
                        // g->SetPoint(g_i++, i, h1->GetBinContent(i - 1) + (j -
                        // 1) * h1_step_size);
                        g->SetPoint(g_i++, hStart + i,
                                    h_g1->Eval(cur_x - step_size));
                        break;
                    }
                }
            }
        }

        // Float_t sign = h1->GetBinContent(hStart) - sw->Eval(0);
        // for (i = 1; i < n; i++) {
        //     Float_t diff = h1->GetBinContent(hStart + i) - sw->Eval(i);

        //     if (sign * diff <= 0) {
        //         Float_t cur = sign;
        //         Float_t step_size = 1 / approx_steps;
        //         // Float_t h1_step_size = (h1->GetBinContent(i) -
        //         // h1->GetBinContent(i - 1)) / approx_steps;
        //         sign = diff;
        //         Float_t cur_x = hStart + i - 1;

        //         int j;

        //         for (j = 1; j < approx_steps; j++) {
        //             Float_t tmp = cur;
        //             cur_x += step_size;
        //             cur = sw->Eval(cur_x + (0.02 * gRandom->Gaus(0, 0.3))) -
        //                   h_g1->Eval(cur_x);
        //             if (abs(tmp) < abs(cur)) {
        //                 // g->SetPoint(g_i++, i, h1->GetBinContent(i - 1) +
        //                 (j -
        //                 // 1) * h1_step_size);
        //                 g->SetPoint(g_i++, i, h_g1->Eval(cur_x - step_size));
        //                 break;
        //             }
        //         }
        //     }
        // }

        delete h_g1;
        return g;
    }
};