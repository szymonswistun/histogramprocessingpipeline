#pragma once

#include "Abstracts.hpp"
#include "TDD2FTab2.hpp"

class Interpolator : public Provider<TObjectPair<TH1F, TGraph>> {
   public:
    void setProvider(Provider<TH1F> *provider) { this->provider = provider; }

    Interpolator(float freq, Provider<TH1F> *provider = nullptr) {
        this->tdd2ftab = new TDD2FTab2(freq, 1, 600);
        this->setProvider(provider);
    }

    bool hasMore() override { return provider->hasMore(); }

   private:
    Provider<TH1F> *provider;
    TDD2FTab *tdd2ftab = nullptr;

    TObjectPair<TH1F, TGraph> *getInt(bool threadSafe = false) override {
        TH1F *h = provider->get();

        if (h) {
            return new TObjectPair<TH1F, TGraph>(h, tdd2ftab->convert(h));
        } else
            return nullptr;
    }
};