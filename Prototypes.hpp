#pragma once

template <typename T> class Provider;
template <typename T1, typename T2> class ProviderWrapper;
template <typename T1, typename T2> class Consumer;
template <typename T1, typename T2> class LambdaConsumer;
template <typename T1, typename T2> class Accumulator;
template <typename T1, typename T2, typename T3> class CompoundConsumer;
template <typename T1, typename T2> class SplitterConsumer;
template <typename T1, typename T2, typename T3> class TerminalConsumer;
template <typename T1, typename T2> class PeekProvider;
template <typename T1, typename T2> class ProviderWithConsumer;
template<typename T> class RotaryBuffer;
template <typename T1, typename T2> class ConsumerFuture;
template <typename T1, typename T2> class Task;
template <typename T1, typename T2> class TaskRunner;
template <typename T1, typename T2> class TaskRunnerMT;
template <typename T1, typename T2> class TaskRunnerST;