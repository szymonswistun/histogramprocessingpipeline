#pragma once

#include <future>
//#include <mutex>
#include <thread>

#include "Abstracts.hpp"
#include "Prototypes.hpp"

using namespace std;

template <typename T1, typename T2>
class ConsumerFuture {
   public:
    ConsumerFuture(Consumer<T1, T2> *consumer, T1 *e, bool threadSafe) {
        if (active) return;
        if (t) delete t;
        void *a[] = {(void *)consumer, (void *)e, (void *)threadSafe,
                     (void *)nullptr};
        args = a;
        t = new TThread(Form("t%lf", gRandom->Gaus(1000, 1000)),
                        ConsumerFuture<T1, T2>::fun0, (void *)args);
        t->Run();

        active = true;
    }

    T2 *get() {
        if (active) {
            t->Join();
            T2 *ret = (T2 *)args[3];
            delete args;
            delete t;
            args = nullptr;
            t = nullptr;
            return ret;
        } else
            return nullptr;
    }

    // ~ConsumerFuture() {
    //     delete t;
    // }

   private:
    TThread *t;
    void **args;
    bool active = false;

    static void *fun0(void *a) {
        void **args = (void **)a;
        args[3] = (void *)fun1((Consumer<T1, T2> *)args[0], (T1 *)args[1],
                               (bool)args[2]);
        return 0;
    }

    static T2 *fun1(Consumer<T1, T2> *consumer, T1 *e, bool threadSafe) {
        return consumer->consume(e, threadSafe);
    }
};

template <typename T1, typename T2>
class Task {
   public:
    Task(Consumer<T1, T2> *consumer) { this->consumer = consumer; }

    T2 *process(T1 *e, bool threadSafe = false) {
        return consumer->consume(e, threadSafe);
    }

    ConsumerFuture<T1, T2> *processAsync(T1 *e, bool threadSafe = true) {
        // // auto l = [&](Consumer<T1, T2> *c) { c->consume(e, threadSafe); };
        // return async([](Consumer<T1, T2> *c, T1* e, bool threadSafe) {
        //     return c->consume(e, threadSafe);
        // }, consumer, e, threadSafe);
        return new ConsumerFuture<T1, T2>(consumer, e, threadSafe);
    }

    TaskRunnerST<T1, T2> *with(Provider<T1> *provider, bool printTime = false) {
        return new TaskRunnerST<T1, T2>(provider, this, printTime);
    }

    TaskRunnerMT<T1, T2> *withMT(Provider<T1> *provider,
                                 bool printTime = false) {
        return new TaskRunnerMT<T1, T2>(provider, this, printTime);
    }

   protected:
    Consumer<T1, T2> *consumer;
};

template <typename T1, typename T2>
class TaskRunner {
   public:
    TaskRunner(Provider<T1> *prov, Task<T1, T2> *task, bool printTime) {
        this->prov = prov;
        this->task = task;
        this->printTime = printTime;
    }

    virtual list<T2 *> *run(int threadCount = 4) { return nullptr; }
    virtual void runVoid(int threadCount = 4) {}

    void setPrintTime(bool printTime) { this->printTime = printTime; }

   protected:
    Provider<T1> *prov;
    Task<T1, T2> *task;
    bool printTime;
};

template <typename T1, typename T2>
class TaskRunnerMT : public TaskRunner<T1, T2> {
   public:
    TaskRunnerMT(Provider<T1> *prov, Task<T1, T2> *task, bool printTime)
        : TaskRunner<T1, T2>(prov, task, printTime) {}

    virtual list<T2 *> *run(int threadCount = 4) override {
        ConsumerFuture<T1, T2> **futureArr =
            new ConsumerFuture<T1, T2> *[threadCount];
        list<T2 *> *retList = new list<T2 *>();

        while (this->prov->hasMore()) {
            for (int i = 0; i < threadCount; i++)
                futureArr[i] = this->task->processAsync(this->prov->get());

            for (int i = 0; i < threadCount; i++) {
                retList->push_back(futureArr[i]->get());
                delete futureArr[i];
            }
        }

        delete[] futureArr;
        return retList;
    }

    virtual void runVoid(int threadCount = 4) override {
        ConsumerFuture<T1, T2> **futureArr =
            new ConsumerFuture<T1, T2> *[threadCount];

        while (this->prov->hasMore()) {
            for (int i = 0; i < threadCount; i++)
                futureArr[i] = this->task->processAsync(this->prov->get());

            for (int i = 0; i < threadCount; i++) {
                futureArr[i]->get();
                delete futureArr[i];
            }
        }

        delete[] futureArr;
    }
};

template <typename T1, typename T2>
class TaskRunnerST : public TaskRunner<T1, T2> {
   public:
    TaskRunnerST(Provider<T1> *prov, Task<T1, T2> *task, bool printTime = false)
        : TaskRunner<T1, T2>(prov, task, printTime) {}

    virtual list<T2 *> *run(int threadCount = 1) override {
        list<T2 *> *retList = new list<T2 *>();

        while (this->prov->hasMore()) {
            T1 *e = this->prov->get();
            if (e) retList->push_back(this->task->process(e));
        }

        return retList;
    }

    virtual void runVoid(int threadCount = 1) override {
        list<T2> *retList = new list<T2>();

        time_t t1, t2;
        TDatime tdat;
        if (this->printTime) t1 = tdat.Convert();

        while (this->prov->hasMore()) {
            T1 *e = this->prov->get();
            if (e != nullptr) this->task->process(e);
        }
        if (this->printTime) {
            tdat.Set();
            t2 = tdat.Convert();
            printf("Time elapsed: %ld\n", t2 - t1);
        }
    }
};
