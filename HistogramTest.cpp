#include <list>
#include "Pipeline.hpp"

using namespace std;

class TempProcessor : public Consumer<TObjectPair<TH1F, TGraph>, Result> {
   public:
    TempProcessor(TH1F* hist) { this->hist = hist; }

   private:
    TH1F* hist;

    Result* consumeInt(TObjectPair<TH1F, TGraph>* p,
                       bool threadSafe = false) override {
        TH1F* h = p->e1;
        float amp = h->GetMaximum();

        Result* res = new Result();
        res->graph = hist->Integral() * amp;
        res->orig = h->Integral();
        res->err = (res->orig - res->graph) / res->orig * 100;
        res->amp = h->GetMaximum();

        res->type = FinderType::Integ;
        return res;
    }
};

void HistogramTest() {
    auto* f = new TFile("norms6.root");

    list<const char*>* names = new list<const char*>();
    for (int i = 0; i < 7; i++)
        names->push_back(Form("data/signals%i.root", i));

    Provider<TObjectPair<TH1F, TGraph>>* provider = new Preprocessor(
        new Interpolator(20, new TObjectProvider<TH1F>(names)));

    TH1F** hists = new TH1F*[11];
    TH2F** hists2 = new TH2F*[11];
    // Consumer<TObjectPair, Result>** processors = new TempProcessor*[11];
    list<Consumer<TObjectPair<TH1F, TGraph>, Result>*> processors;

    auto* l = f->GetListOfKeys();

    {
        int i = 0;
        for (auto* k : *l) {
            hists[i] = (TH1F*)f->Get(k->GetName());
            hists[i] = Utils::extractTemplatePeak(hists[i]);
            hists[i] = Utils::zeroAlignAndNormalizeInPlace(hists[i]);

            const char* name = Form("%s-th2f", k->GetName());
            TempProcessor* p = new TempProcessor(hists[i]);
            hists2[i] = new TH2F(name, name, 800, 20, 200, 2000, -60, 20);
            Histogram2Accumulator* acc = new Histogram2Accumulator(
                hists2[i], Histogram2Accumulator::AMP);
            // processors[i] = acc->after(p);
            processors.push_back(acc->after(p));

            i++;
        }
    }

    auto* splitter = new SplitterConsumer<TObjectPair<TH1F, TGraph>, Result>(
        &processors, false, true);

    auto* runner = splitter->asTask()->with(provider);
    runner->setPrintTime(true);
    runner->runVoid();

    for (int i = 0; i < 11; i++) {
        auto* c = new TCanvas("c", "", 1200, 600);
        hists2[i]->Draw();
        if (i == 0)
            c->Print("out/histsTest.pdf(");
        else
            c->Print("out/histsTest.pdf");

        TH1D *hM, *hS;
        Utils::fitSlicesY(hists2[i], hM, hS);
        c->Clear();
        c->Divide(2);
        c->cd(1);
        hM->Draw();
        c->cd(2);
        hS->Draw();

        if (i == 10)
            c->Print("out/histsTest.pdf)");
        else
            c->Print("out/histsTest.pdf");
    }
}