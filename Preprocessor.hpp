#pragma once

#include "Utils.hpp"

class Preprocessor : public Provider<TObjectPair<TH1F, TGraph>> {
   public:
    Preprocessor(Provider<TObjectPair<TH1F, TGraph>>* provider,
                 double maxAmp = -1) {
        this->provider = provider;
        this->maxAmp = maxAmp;
    }

    bool hasMore() override { return provider->hasMore(); }

   private:
    Provider<TObjectPair<TH1F, TGraph>>* provider;
    double maxAmp;

    TObjectPair<TH1F, TGraph>* getInt(bool threadSafe = false) override {
        TObjectPair<TH1F, TGraph>* p = this->provider->get();
        if (!p) return nullptr;
        // if (maxAmp > 0 && p->e1->GetMaximum() > maxAmp) return nullptr;

        p = Utils::extractPeakInPlace(p);

        if (p->e1 == nullptr || p->e2 == nullptr) {
            delete p;
            return nullptr;
        } else if (!Utils::validate(p->e1)) {
            p->e1->Delete();
            p->e2->Delete();
            return nullptr;
        }

        Utils::zeroAlignInPlace(p->e1);

        p->e1->SetBinContent(0, 0);
        p->e1->SetBinContent(p->e1->GetSize() - 1, 0);

        return p;
    }
};
