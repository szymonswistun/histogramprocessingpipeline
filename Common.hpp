#pragma once

#ifdef __GNUC__
#include "CompileIncludes.h"
#endif

enum FinderType { Simple, Root, Integ, Mean };

typedef struct {
    double orig;
    double graph;
    double err;
    double amp;

    FinderType type;
} Result;
