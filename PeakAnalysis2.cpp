#include "Pipeline.hpp"

using namespace std;

void PeakAnalysis2() {
    list<const char*>* names = new list<const char*>();
    for (int i = 0; i < 7; i++)
        names->push_back(Form("data/signals%i.root", i));

    auto colorL = [](TObjectPair<TH1F, TGraph>* p) {
        // p->e1->SetLineColor(kBlue);

        p->e2->SetLineColor(kBlack);
        // p->e2->SetMarkerColor(kRed);

        return p;
    };

    Consumer<TObjectPair<TH1F, TGraph>, TObjectPair<TH1F, TGraph>>* color =
        new LambdaConsumer<TObjectPair<TH1F, TGraph>,
                           TObjectPair<TH1F, TGraph>>(colorL);

    Provider<TObjectPair<TH1F, TGraph>>* provider =
        new Interpolator(20, new TObjectProvider<TH1F>(names));
    provider = new Preprocessor(provider, 300);
    // provider = provider->with(color);

    auto* pdfAcc = new PdfAccumulator(3, 3, true);

    auto drawL = [](TObjectPair<TH1F, TGraph>* p) {
        TH1F* h = p->e1;
        TGraph* g = p->e2;

        double xMax, yMax;
        Utils::getMaxPointAdjusted(xMax, yMax, h);

        TCanvas* c = new TCanvas("c", "", 400, 400);
        h->DrawCopy();
        g->DrawClone("L");

        TLine* l1 = new TLine(xMax, 0, xMax, 1000);
        l1->SetLineColor(kRed);
        l1->DrawClone("same");

        TLine* l2 = new TLine(0, yMax, 1200, yMax);
        l2->SetLineColor(kGreen);
        l2->DrawClone("same");

        delete p;
        delete l1, l2;
        return c;
    };

    Consumer<TObjectPair<TH1F, TGraph>, TCanvas>* draw =
        new LambdaConsumer<TObjectPair<TH1F, TGraph>, TCanvas>(drawL);
    draw = draw->after(color);
    draw = draw->before(pdfAcc);

    TH1F* diffsX = new TH1F("diffsX", "", 1000, -150, 50);
    TH2F* diffsY = new TH2F("diffsY-amp", "", 1000, -100, 100, 500, 40, 280);

    HistogramAccumulator* accX = new HistogramAccumulator(diffsX);
    // Consumer<Pair<double, double>, Pair<double, double>>* pSplit =
    //     new PairSplitterConsumer(accX, accY);
    double params[] = {-0.00000036, 0.00009405, 0.00000000, 0.00000000, 50};

    auto diffsL = [&](TObjectPair<TH1F, TGraph>* p) {
        TH1F* h = p->e1;
        TGraph* g = p->e2;

        double hXmax, hYmax, gXmax, gYmaxOld, gYmax;
        Utils::getMaxPointAdjusted(hXmax, hYmax, h);
        Utils::getMaxPoint(gXmax, gYmaxOld, g);
        // gYmax *= 1.1;
        // gYmax *= 1.1 + (gYmax / 8000);
        // gYmax *= 1.0 + gYmax * 0.0008 + gYmax * gYmax * 0.000004;
        gYmax = gYmaxOld *
                    (gYmaxOld * (gYmaxOld * (gYmaxOld * params[0] + params[1]) +
                                 params[2]) +
                     params[3]) +
                params[4];
        // amp * (amp * (amp * (amp * params[0] + params[1]) + params[2]) +
        // params[3]) + 1 gXmax *= 1.15;

        double *xDiff = new double, *yDiff = new double;
        *xDiff = hXmax - gXmax;
        *yDiff = hYmax - gYmax;

        accX->consume(xDiff);
        diffsY->Fill(*yDiff, hYmax);

        return p;
    };

    Consumer<TObjectPair<TH1F, TGraph>, TObjectPair<TH1F, TGraph>>* diffs =
        new LambdaConsumer<TObjectPair<TH1F, TGraph>,
                           TObjectPair<TH1F, TGraph>>(diffsL);

    draw = draw->after(diffs);

    // auto* canvasProvider = provider->with<TCanvas>(draw);

    auto* task = draw->asTask();
    auto* runner = task->with(provider);

    gSystem->RedirectOutput("/dev/null");
    runner->runVoid();
    pdfAcc->finalize();
    gSystem->RedirectOutput(nullptr);

    TCanvas* c = new TCanvas("diffsC", "", 1200, 600);
    c->Divide(2);

    c->cd(1);
    diffsX->Draw();

    c->cd(2);
    diffsY->Draw();

    new TFile("hist.root", "RECREATE");
    diffsY->Write();
    gFile->Close();

    printf("diffsY mean x: %lf\n", diffsY->GetMean(1));

    c->SaveAs("diffs.png");
}