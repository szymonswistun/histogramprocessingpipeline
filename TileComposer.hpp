#pragma once

#include <list>

#include "Abstracts.hpp"
#include "Common.hpp"

using namespace std;

class TileComposer {
   private:
    TCanvas* c;
    int nCols, nRows;
    const char* pdfName;

    int x = 0, y = 0, pageCount = 0;

    int calculateCd() { return 1 + y * nRows + x; }

    int calculateCd(int x, int y) { return 1 + y * nRows + x; }

    void flush() {
        if (pageCount == 0)
            c->Print(Form("%s.pdf(", pdfName));
        else
            c->Print(Form("%s.pdf", pdfName));

        pageCount++;
    }
    void setFrame() { c->cd(calculateCd()); }

   public:
    TileComposer(int nCols, int nRows, const char* pdfName) {
        this->nCols = nCols;
        this->nRows = nRows;
        this->pdfName = pdfName;

        c = new TCanvas(pdfName, pdfName, 1200, 1200);
        c->Divide(nCols, nRows);
    }

    void draw(TObject* obj, const char* flags = "") {
        auto* pad = gPad;
        setFrame();
        obj->Draw(flags);
        gPad = pad;
    }

    void nextFrame() {
        // c->WaitPrimitive();
        if (calculateCd() >= nCols * nRows)
            nextPage();
        else if (x >= nCols) {
            x = 0;
            y += 1;
        } else
            x += 1;
    }

    void nextPage() {
        flush();
        c->Clear();
        c->Divide(nCols, nRows);
        x = y = 0;
    }

    void close() { c->Print(Form("%s.pdf)", pdfName)); }
};