#include "IntegralTest.cpp"
#include "MeanTest.cpp"
#include "MeanTest2.cpp"
#include "Pipeline.hpp"
#include "RootTest.cpp"

#include <list>

using namespace std;

void ComposeResults(int n = 4) {
    list<const char *> imgs;

    imgs.push_front(MeanTest()->getImg());
    imgs.push_front(MeanTest2()->getImg());
    imgs.push_front(IntegralTest()->getImg());
    imgs.push_front(RootTest()->getImg());

    TImage *img;
    TCanvas *c = new TCanvas("reports", "", 1200, 1200);

    int i = 0;
    for (auto *pth : imgs) {
        c->Clear();
        img = TImage::Open(pth);
        img->Draw("xxx");
        if (i == 0)
            c->Print("results.pdf(");
        else if (i == n - 1)
            c->Print("results.pdf)");
        else
            c->Print("results.pdf");

        i++;
        img->Delete();
    }
}