#pragma once

#include <list>
#include <type_traits>

#include "Common.hpp"
#include "Prototypes.hpp"

using namespace std;

class Point {
   public:
    Point(double x, double y) {
        this->x = x;
        this->y = y;
    }

    double x, y;
};

template <typename T1, typename T2>
class Pair {
   public:
    Pair(T1* e1, T2* e2) {
        this->e1 = e1;
        this->e2 = e2;
    }

    T1* e1;
    T2* e2;

    virtual ~Pair() {}
};

template <typename T1, typename T2>
class TObjectPair : public Pair<T1, T2> {
    static_assert(std::is_convertible<T1, TObject>::value &&
                      std::is_convertible<T2, TObject>::value,
                  "TObjectPair works only with TObjects");

   public:
    TObjectPair(T1* e1, T2* e2) : Pair<T1, T2>(e1, e2) {}

    TObjectPair* clone() {
        T1* e1 = (this->e1) ? (T1*)this->e1->Clone() : e1;
        T2* e2 = (this->e2) ? (T2*)this->e2->Clone() : e2;
        return new TObjectPair(e1, e2);
    }

    virtual ~TObjectPair() {
        if (this->e1) this->e1->Delete();
        if (this->e2) this->e2->Delete();
    }
};

template <typename T>
class Provider {
   public:
    virtual T* get(bool threadSafe = false) {
        // printf("Virtual get\n");
        //        if (threadSafe) lock.lock();
        T* ret = getInt();
        //        if (threadSafe) lock.unlock();
        return ret;
    }

    virtual bool hasMore() { return false; }

    template <typename T2>
    Provider<T2>* with(Consumer<T, T2>* c) {
        return new ProviderWithConsumer<T, T2>(this, c);
    }

   protected:
    //    std::mutex lock;

    virtual T* getInt(bool threadSafe = false) {
        printf("Virtual getInt\n");
        return nullptr;
    }
};

template <typename T1, typename T2>
class ProviderWrapper : public Provider<T2> {
   public:
    ProviderWrapper(T1* p) { this->p = p; }

    virtual T2* get(bool threadSafe = false) override {
        T2* e = p->get();
        return (T2*)e;
    }

   private:
    T1* p;
};

template <typename T1, typename T2>
Provider<T2>* wrapProvider(T1* p) {
    return (Provider<T2>*)new ProviderWrapper<T1, T2>(p);
}

template <typename T>
class LimitedProvider : public Provider<T> {
   public:
    LimitedProvider(Provider<T>* p, int n) {
        this->p = p;
        this->n = n;
    }

    bool hasMore() override { return p->hasMore() && n > 0; }

   private:
    Provider<T>* p;
    int n;

    T* getInt(bool threadSafe = false) override {
        n--;
        return p->get(threadSafe);
    }
};

template <typename T1, typename T2>
class Consumer {
   public:
    virtual T2* consume(T1* e, bool threadSafe = false) {
        // printf("Virtual consume\n");
        //       if (threadSafe) lock.lock();
        T2* ret = consumeInt(e);
        //       if (threadSafe) lock.unlock();
        return ret;
    }

    virtual void finalize() {}

    template <typename T3>
    Consumer<T3, T2>* after(Consumer<T3, T1>* c) {
        return new CompoundConsumer<T3, T1, T2>(c, this);
    }

    template <typename T3>
    Consumer<T1, T3>* before(Consumer<T2, T3>* c) {
        return new CompoundConsumer<T1, T2, T3>(this, c);
    }

    Task<T1, T2>* asTask() { return new Task<T1, T2>(this); }

    SplitterConsumer<T1, T2>* nextTo(Consumer<T1, T2>* c, bool copy = false) {
        if (!c) return nullptr;
        list<Consumer<T1, T2>*>* cL = new list<Consumer<T1, T2>*>();
        cL->push_back(this);
        cL->push_back(c);
        return new SplitterConsumer<T1, T2>(cL, copy);
    }

    SplitterConsumer<T1, T2>* nextTo(list<Consumer<T1, T2>*> cL,
                                     bool copy = true) {
        if (!cL) return nullptr;
        return new SplitterConsumer<T1, T2>(cL, copy);
    }

    SplitterConsumer<T1, T2>* operator+(Consumer<T1, T2>* c) {
        return nextTo(c);
    }

   protected:
    //   std::mutex lock;

    virtual T2* consumeInt(T1* e, bool threadSafe = false) {
        printf("Virtual consumeInt\n");
        return nullptr;
    }
};

template <typename T1, typename T2>
class LambdaConsumer : public Consumer<T1, T2> {
   public:
    LambdaConsumer(function<T2*(T1*)> lambda) { this->lambda = lambda; }

   private:
    function<T2*(T1*)> lambda;

    virtual T2* consumeInt(T1* e, bool threadSafe = false) override {
        return lambda(e);
    }
};

template <typename T1, typename T2>
class Accumulator : public Consumer<T1, T1> {
   public:
    Accumulator(T2* acc) { this->acc = acc; }

    T1* consume(T1* e, bool threadSafe = false) override {
        accumulate(e);
        return e;
    }

    virtual T2* getResult() { return acc; }

    template <typename T>
    TerminalConsumer<T1, T1, T>* toTerminalConsumer() {
        return new TerminalConsumer<T1, T1, T>(this);
    }

   protected:
    T2* acc;

    virtual void accumulate(T1* e, bool threadSafe = false) {}
};

template <typename T1, typename T2, typename T3>
class CompoundConsumer : public Consumer<T1, T3> {
   public:
    CompoundConsumer(Consumer<T1, T2>* c1, Consumer<T2, T3>* c2) {
        this->c1 = c1;
        this->c2 = c2;
    }

    virtual void finalize() override {
        this->c1->finalize();
        this->c2->finalize();
    }

   protected:
    Consumer<T1, T2>* c1;
    Consumer<T2, T3>* c2;

    T3* consumeInt(T1* e1, bool threadSafe = false) override {
        T2* e2 = c1->consume(e1, threadSafe);
        return c2->consume(e2, threadSafe);
    }
};

template <typename T1, typename T2>
class SplitterConsumer : public Consumer<T1, std::list<T2*>> {
   public:
    SplitterConsumer(std::list<Consumer<T1, T2>*>* l, bool copy = false,
                     bool cleanup = false) {
        this->l = l;
        this->copy = copy;
        this->cleanup = cleanup;
    }

    virtual void finalize() override {
        for (auto* c : *(this->l)) c->finalize();
    }

    void setCopy(bool copy) { this->copy = copy; }

    void setCleanup(bool cleanup) { this->cleanup = cleanup; }

    SplitterConsumer<T1, T2>* add(Consumer<T1, T2>* c) {
        l->push_back(c);
        return this;
    }

    SplitterConsumer<T1, T2>* with(Consumer<T1, T2>* c) { return add(c); }

    SplitterConsumer<T1, T2>* operator+=(Consumer<T1, T2>* c) { return add(c); }

   protected:
    std::list<Consumer<T1, T2>*>* l;
    bool copy, cleanup;

    std::list<T2*>* consumeInt(T1* e1, bool threadSafe = false) override {
        std::list<T2*>* retList = new std::list<T2*>();

        if (e1) {
            for (auto* c : *l) {
                T1* e = (copy) ? e->clone() : e1;
                retList->push_back(c->consume(e, threadSafe));
            }
            if (cleanup) delete e1;
        }

        return retList;
    }
};

template <typename T1, typename T2, typename T3, typename T4>
class PairSplitterConsumer : public Consumer<Pair<T1, T2>, Pair<T3, T4>> {
   public:
    PairSplitterConsumer(Consumer<T1, T3>* c1, Consumer<T2, T4>* c2,
                         bool cleanup = false) {
        this->c1 = c1;
        this->c2 = c2;
        this->cleanup = cleanup;
    }

    virtual void finalize() override {
        this->c1->finalize();
        this->c2->finalize();
    }

   private:
    Consumer<T1, T3>* c1;
    Consumer<T2, T4>* c2;
    bool cleanup = false;

    Pair<T3, T4>* consumeInt(Pair<T1, T2>* p,
                             bool threadSafe = false) override {
        T3* ret1 = c1->consume(p->e1);
        T4* ret2 = c2->consume(p->e2);
        if (cleanup) delete p;

        return new Pair<T3, T4>(ret1, ret2);
    }
};

template <typename T1, typename T2, typename T3>
class TerminalConsumer : public Consumer<T1, T3> {
   public:
    TerminalConsumer(Consumer<T1, T2>* c) { this->c = c; }

    T3* consumeInt(T1* e1, bool threadSafe = false) {
        c->consume(e1);
        return nullptr;
    }

    virtual void finalize() override { this->c->finalize(); }

   private:
    Consumer<T1, T2>* c;
};

template <typename T1, typename T2>
class PeekProvider : public Provider<T1> {
   public:
    PeekProvider(Provider<T1>* prov, Consumer<T1, T2>* c) {
        this->prov = prov;
        this->c = c;
    }

    T1* getInt(bool threadSafe = false) override {
        T1* p = prov->get();
        if (p) c->consume(p);

        printf("%s\n", p->e1->GetName());

        return p;
    }

   private:
    Provider<T1>* prov;
    Consumer<T1, T2>* c;
};

template <typename T1, typename T2>
class ProviderWithConsumer : public Provider<T2> {
   public:
    ProviderWithConsumer(Provider<T1>* p, Consumer<T1, T2>* c) {
        this->p = p;
        this->c = c;
    }

   private:
    Provider<T1>* p;
    Consumer<T1, T2>* c;

    T2* getInt(bool threadSafe = false) override {
        T1* e = p->get();
        return c->consume(e);
    }
};

template <typename T>
class RotaryBuffer {
   public:
    RotaryBuffer(int n = 10) {
        this->n = n;
        this->buf = new T*[n];
    }

    void push(T* i) {
        buf[i_w++] = i;
        afterOp();
    }

    T* pop() {
        T* r = buf[i_r++];
        afterOp();
        return r;
    }

    void setToOldest() { i_r = (i_w - n) % n; }

   private:
    T** buf;
    int n;
    int i_r = 0, i_w = 0;

    void afterOp() {
        i_r %= n;
        i_w %= n;
    }
};
