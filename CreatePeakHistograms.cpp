#include "Utils.hpp"

void CreatePeakHistograms(
    TDirectory *hists = (TDirectory *)_file0->Get("hists"),
    TDirectory *graphs = (TDirectory *)_file0->Get("graphs"),
    TFile *fOut = new TFile("hPeaks.root", "RECREATE")) {
    TList *hL = hists->GetListOfKeys();
    TList *gL = graphs->GetListOfKeys();

    TH1D *hH = new TH1D("Histogram peaks", "", hL->GetSize(), 0, hL->GetSize());
    TH1D *gH = new TH1D("Graph peaks", "", hL->GetSize(), 0, hL->GetSize());

    fOut->cd();

    int i = 0;
    for (auto *k : *hL) {
        TH1D *h = (TH1D *)hists->Get(k->GetName());
        TGraph *g = (TGraph *)graphs->Get(k->GetName());

        if (!(g && h)) continue;

        double xG, yG, yH = h->GetMaximum();
        if (yH < 280) {
            hH->SetBinContent(i, yH);
            Utils::getMaxPoint(xG, yG, g);
            gH->SetBinContent(i++, yG);
        }
    }
    hH->Write();
    gH->Write();
}