#pragma once

#include "Abstracts.hpp"

template <typename T>
class PassthroughConsumer : Consumer<TObjectPair<TH1F, TGraph>, T> {
    public:
        PassthroughConsumer(Consumer<TObjectPair<TH1F, TGraph>, T> *c) {
            this->c = c;
        }

    private:
        Consumer<TObjectPair<TH1F, TGraph>, T> *c;

        T* consumeInt(TObjectPair<TH1F, TGraph> *e) {
            printf("%s - %s\n", e->e1->GetName(), e->e2->GetName());
            return c->consume(e);
        }
};