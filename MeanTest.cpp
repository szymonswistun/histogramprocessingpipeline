#include "MeanCurveFinder.hpp"
#include "Pipeline.hpp"
#include "ReportFormatter.hpp"

#define FILE_COUNT 7

ReportFormatter *MeanTest(int i = 0, float freq = 20) {
    int n;
    float *amps;
    TH1F **templates = Utils::getTemplates(new TFile("norms6.root"), n, amps);
    for (int i = 0; i < n; i++)
        templates[i] = Utils::zeroAlignAndNormalizeInPlace(
            Utils::extractTemplatePeak(templates[i]));

    Consumer<TObjectPair<TH1F, TGraph>, Result> *test =
        new MeanCurveFinder(templates, amps, n);

    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < FILE_COUNT; i++)
        names->push_back(Form("data/signals%i.root", i));
    Provider<TObjectPair<TH1F, TGraph>> *provider = new Preprocessor(
        new Interpolator(freq, new TObjectProvider<TH1F>(names)));
    // provider = new LimitedProvider<TObjectPair<TH1F, TGraph>>(provider, 250);

    // Consumer<Result, Result> *formatter =
    // new ReportFormatter("", 'R', 20, 20, 200, 4000, 30000, 30, -0.05,
    // 0.05);
    ReportFormatter *formatter =
        new ReportFormatter("", 'M', 20, 20, 200, 4000, 30000, 1000, -500, 500);

    test = test->before(formatter);

    auto lambdaP = [](Result *r) {
        if (r) printf("Amp: %.2f, Err: %.5f\n", r->amp, r->err);
        return r;
    };

    Consumer<Result, Result> *lambdaPC =
        new LambdaConsumer<Result, Result>(lambdaP);
    // test = test->before(lambdaPC);

    int cnt = 0;
    auto lambdaS = [&](Result *r) {
        if (r != nullptr) printf("\r%i/%i", ++cnt, FILE_COUNT * 1000);
        return r;
    };
    Consumer<Result, Result> *lambdaSC =
        new LambdaConsumer<Result, Result>(lambdaS);
    test = test->before(lambdaSC);

    auto *runner = test->asTask()->with(provider);

    clock_t tStart = clock();
    runner->runVoid();
    printf("Exec time: %.8lf\n", (clock() - tStart) * 1.0 / CLOCKS_PER_SEC);

    test->finalize();
    return formatter;
}
