#include "Abstracts.hpp"
#include "Common.hpp"

class HistogramAccumulator : public Accumulator<double, TH1F> {
   public:
    HistogramAccumulator(TH1F* h) : Accumulator(h) {}

   private:
    void accumulate(double* e, bool threadSafe = false) override {
        // printf("Histogram2Accumulator\n");
        if (e != nullptr) {
            this->acc->Fill(*e);
        }
    }
};