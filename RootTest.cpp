#include "Pipeline.hpp"
#include "ReportFormatter.hpp"
#include "RootCurveFinder.hpp"

#define FILE_COUNT 7

ReportFormatter *RootTest(int i = 0, float freq = 20) {
    printf("Started for freq=%.2fMHz\n", freq);
    int n;
    float *amps;
    TH1F **templates = Utils::getTemplates(new TFile("norms6.root"), n, amps);
    for (int i = 0; i < n; i++)
        templates[i] = Utils::zeroAlignAndNormalizeInPlace(
            Utils::extractTemplatePeak(templates[i]));

    Consumer<TObjectPair<TH1F, TGraph>, Result> *test =
        new RootCurveFinder(templates, amps, n, true);

    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < FILE_COUNT; i++)
        names->push_back(Form("data/signals%i.root", i));
    Provider<TObjectPair<TH1F, TGraph>> *provider = new Preprocessor(
        new Interpolator(freq, new TObjectProvider<TH1F>(names)));

    ReportFormatter *formatterB =
        new ReportFormatter("", 'R', 20, 20, 200, 4000, 30000, 100, -100, 100);
    ReportFormatter *formatterN =
        new ReportFormatter("", 'Q', 20, 20, 200, 4000, 30000, 40, -40, 40);

    int cnt = 0;
    auto lambdaS = [&](Result *r) {
        printf("\r%i/%i", ++cnt, FILE_COUNT * 1000);
        return r;
    };
    Consumer<Result, Result> *lambdaSC =
        new LambdaConsumer<Result, Result>(lambdaS);
    test = test->before(lambdaSC);

    test = test->before(formatterB);
    test = test->before(formatterN);

    TH1F *errs = new TH1F("errs", "errs", 50, -100, 100);

    auto lambdaE = [&](Result *r) {
        errs->Fill(r->err);
        return r;
    };
    Consumer<Result, Result> *lambdaEC =
        new LambdaConsumer<Result, Result>(lambdaE);
    test = test->before(lambdaEC);

    auto *runner = test->asTask()->with(provider);

    clock_t tStart = clock();
    runner->runVoid();
    printf("\nExec time: %.8lf\n", (clock() - tStart) * 1.0 / CLOCKS_PER_SEC);

    printf("Finalizing\n");
    test->finalize();
    printf("Finalized\n");

    TF1 *fG = new TF1("fG", "gaus", -100, 100);
    errs->Fit(fG);
    TCanvas *c2 = new TCanvas();
    errs->Draw();
    c2->SaveAs("out/fittingErrs.png");

    return formatterB;
}

#ifdef __GNUC__
int main(int argc, char **argv) {
    TApplication App(argv[0], &argc, argv);
    RootTest();
    return 0;
}
#endif