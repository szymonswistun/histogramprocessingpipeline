#include "MeanTest.hpp"
#include "Pipeline.hpp"
#include "ReportFormatter.hpp"

ReportFormatter *MeanTest2(int i = 0) {
    Consumer<TH1F, Result> *test = new class MeanTest();
    auto lambda = [](TH1F *h) {
        if (!h) return (TH1F *)nullptr;

        Utils::subtractBaselineInPlace(h);
        TH1F *hC = Utils::extractPeak(h);
        // h->Delete();

        float integ = hC->Integral();
        if (integ > 5000 && integ < 30000)
            return hC;
        else
            return (TH1F *)nullptr;
    };

    Consumer<TH1F, TH1F> *lambdaC = new LambdaConsumer<TH1F, TH1F>(lambda);

    int n;
    TH1F **templates = Utils::getTemplates(new TFile("norms6.root"), n);
    for (int i = 0; i < n; i++)
        templates[i] = Utils::zeroAlignAndNormalizeInPlace(
            Utils::extractTemplatePeak(templates[i]));

    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < 7; i++)
        names->push_back(Form("data/signals%i.root", i));
    Provider<TH1F> *provider = new TObjectProvider<TH1F>(names);
    // provider = new LimitedProvider<TH1F>(provider, 250);

    ReportFormatter *formatter =
        new ReportFormatter("", 'N', 20, 20, 200, 4000, 30000, 30, -0.05, 0.05);
    // Consumer<Result, Result> *formatter =
    //     new ReportFormatter("", 'M', 20, 20, 200, 4000, 30000, 200, -100,
    //     100);

    test = test->after(lambdaC);
    test = test->before(formatter);

    // auto lambdaP = [](Result *r) {
    //     if (r) printf("Amp: %.2f, Err: %.5f\n", r->amp, r->err);
    //     return r;
    // };

    // Consumer<Result, Result> *lambdaPC =
    //     new LambdaConsumer<Result, Result>(lambdaP);
    // test = test->before(lambdaPC);

    auto *runner = test->asTask()->with(provider);
    runner->runVoid();

    test->finalize();
    return formatter;
}
