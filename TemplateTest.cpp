#include "Utils.hpp"

void TemplateTest(TDirectory* tDir = (TDirectory*)new TFile("norms6.root")) {
    int n;
    TH1F** templates = Utils::getTemplates(tDir, n);

    TH1F* h = new TH1F("norms", "", n, 0, n);

    double minTi = 9999999999, maxTi = -1000;
    for (int i = 0; i < n; i++) {
        double tI = templates[i]->Integral();
        h->SetBinContent(i, tI);
        minTi = (minTi > tI) ? tI : minTi;
        maxTi = (maxTi < tI) ? tI : maxTi;
    }

    printf("Max: %lf, min: %lf\n", maxTi, minTi);
    printf("Diff: %lf - %lf%%\n", maxTi - minTi, (maxTi - minTi) / minTi * 100);

    for (int i = 0; i < n; i++) {
        double bin = h->GetBinContent(i) - minTi;
        h->SetBinContent(i, bin);
    }

    h->Draw();
}
