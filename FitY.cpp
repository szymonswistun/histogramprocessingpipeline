#include "Utils.hpp"

class Approx {
   public:
    Approx(TH1D *gPeaks, int n) {
        this->gPeaks = gPeaks;
        this->n = n;
    }

    double operator[](int i) { return gPeaks->GetBinContent(i); }
    double operator()(double *x, double *params) {
        double amp = gPeaks->GetBinContent(x[0]);
        return amp * (amp * (amp * (amp * params[0] + params[1]) + params[2]) +
                      params[3]) +
               1;
    }

   private:
    TH1D *gPeaks;
    int n;
};

void FitY() {
    TFile *f = new TFile("hPeaks.root");
    TH1D *hPeaks = (TH1D *)f->Get("Histogram peaks");
    TH1D *gPeaks = (TH1D *)f->Get("Graph peaks");

    Approx a(gPeaks, 3);

    // gYmax *= 1.0 + gYmax * 0.0008 + gYmax * gYmax * 0.000004;
    TF1 *fun = new TF1("approx", a, 0, gPeaks->GetSize(), 2);
    // fun->SetParameters(0.000004, 0.0008);

    int res = hPeaks->Fit(fun, "", "", 0, gPeaks->GetSize());

    printf("a=%.8lf, b=%.8lf, c=%.8lf, d=%.8lf, e=1\n", fun->GetParameter(0),
           fun->GetParameter(1), fun->GetParameter(2), fun->GetParameter(3));

    printf("{%.8lf, %.8lf, %.8lf, %.8lf, 1}\n", fun->GetParameter(0),
           fun->GetParameter(1), fun->GetParameter(2), fun->GetParameter(3));
    hPeaks->Draw();
}