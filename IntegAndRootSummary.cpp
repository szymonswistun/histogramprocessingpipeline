#include "IntegralCurveFinder.hpp"
#include "Pipeline.hpp"
#include "RootCurveFinder.hpp"

#define FILE_COUNT 7

Pair<TH2F, TH2F> *IntegAndRootSummaryGen(float freq) {
    int n;
    float *amps;
    TH1F **templates = Utils::getTemplates(new TFile("norms6.root"), n, amps);
    for (int i = 0; i < n; i++)
        templates[i] = Utils::zeroAlignAndNormalizeInPlace(
            Utils::extractTemplatePeak(templates[i]));

    Consumer<TObjectPair<TH1F, TGraph>, Result> *integ =
        new IntegralCurveFinder();

    Consumer<TObjectPair<TH1F, TGraph>, Result> *root =
        new RootCurveFinder(templates, amps, n, false);

    TH2F *integH =
             new TH2F(Form("integ-%.1f", freq), Form("Integral-%.1f", freq), 50,
                      20, 200, 80, -40, 40),
         *rootH = new TH2F(Form("root-%.1f", freq), Form("Root-%.1f", freq), 50,
                           20, 200, 80, -40, 40);

    Consumer<Result, Result> *integC = new LambdaConsumer<Result, Result>(
                                 [&](Result *r) {
                                     integH->Fill(r->amp, r->err);
                                     return r;
                                 }),
                             *rootC = new LambdaConsumer<Result, Result>(
                                 [&](Result *r) {
                                     rootH->Fill(r->amp, r->err);
                                     return r;
                                 });

    integ = integ->before(integC);
    root = root->before(rootC);

    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < FILE_COUNT; i++)
        names->push_back(Form("data/signals%i.root", i));
    Provider<TObjectPair<TH1F, TGraph>> *provider = new Preprocessor(
        new Interpolator(freq, new TObjectProvider<TH1F>(names)));

    auto *task = integ->nextTo(root)->asTask();
    task->with(provider)->runVoid();

    return new Pair<TH2F, TH2F>(integH, rootH);
}

void IntegAndRootSummary() {
    TCanvas *c = new TCanvas("c", "", 1200, 600);
    for (int i = 10; i <= 100; i += 10) {
        c->Clear();
        auto *pair = IntegAndRootSummaryGen(i);
        c->Divide(2);
        c->cd(1);
        pair->e1->Draw("colz");
        c->cd(2);
        pair->e2->Draw("colz");

        if (i == 10)
            c->Print("out/IandRSummary.pdf(");
        else if (i == 100)
            c->Print("out/IandRSummary.pdf)");
        else
            c->Print("out/IandRSummary.pdf");

        delete pair->e1;
        delete pair->e2;
    }
}

#ifdef __GNUC__
int main(int argc, char **argv) {
    TApplication App(argv[0], &argc, argv);
    IntegAndRootSummary();
    return 0;
}
#endif