#include "Abstracts.hpp"
#include "Common.hpp"

class PdfAccumulator : public Accumulator<TCanvas, TCanvas> {
   public:
    PdfAccumulator(int w = 3, int h = 3, bool cleanup = false,
                   const char* name = "data.pdf",
                   TCanvas* c = new TCanvas("data", "", 1200, 1200))
        : Accumulator(c) {
        this->w = w;
        this->h = h;
        this->cleanup = cleanup;
        this->max = w * h;
        this->name = name;

        c->Clear();
        c->Divide(w, h);
    }

    void finalize() override { this->acc->Print(Form("%s)", name), "pdf"); }

   private:
    bool started = false;
    int w, h;
    int max, cur = 1;
    bool cleanup;
    const char* name = "data.pdf";

    void accumulate(TCanvas* e, bool threadSafe = false) override {
        if (!e) return;

        TCanvas* c = this->acc;
        if (cur > max) {
            cur = 1;
            if (started)
                c->Print(name, "pdf");
            else {
                c->Print(Form("%s(", name), "pdf");
                started = true;
            }

            c->Clear();
            c->Divide(w, h);
        } else {
            c->cd(cur++);
            e->DrawClonePad();
        }

        if (cleanup) delete e;
    }
};