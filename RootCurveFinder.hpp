#pragma once

#include <cstdio>
#include "Abstracts.hpp"
#include "Common.hpp"
#include "TileComposer.hpp"

using namespace std;

class ScaledTemplate {
    TH1F *gTemplate;

   public:
    ScaledTemplate(TH1F *gTemplate) { this->gTemplate = gTemplate; }

    double operator()(double *x, double *par) {
        if (gTemplate)
            return par[0] * gTemplate->GetBinContent(
                                TMath::Nint(x[0] * par[2] - par[1]));
        else
            return 0;
    }
};

class RootCurveFinder : public Consumer<TObjectPair<TH1F, TGraph>, Result> {
   private:
    TH1F **templates;
    float *amps;
    TH1F *gTemplate;
    int nTemplates;
    TFile *out;
    bool testMode;

    TileComposer *belowTC, *aboveTC;

   public:
    RootCurveFinder(TH1F **templates, float *amps, int n,
                    bool testMode = false) {
        this->templates = templates;
        this->amps = amps;
        this->nTemplates = n;
        this->out = new TFile("fit.root", "RECREATE");
        this->testMode = testMode;

        if (testMode) {
            belowTC = new TileComposer(5, 5, "absErrBelow20");
            aboveTC = new TileComposer(5, 5, "absErrAbove20");
        }
    }

    TH1F *ScaleTemplate(TH1F *t, float amp) {
        TH1F *t2 = new TH1F(*t);

        int n = t->GetSize();
        for (int i = 0; i < n; i++) {
            float bin = t->GetBinContent(i) * amp;
            t2->SetBinContent(i, bin);
        }

        return t2;
    }

    int getClosest(float v) {
        int i;
        for (int i = 1; i < nTemplates; i++) {
            if (amps[i] > v) return i - 1;
        }
        return nTemplates - 1;
    }

    TH1F *FindTemplate(float v) { return templates[getClosest(v)]; }
    // TH1F *FindTemplate(float v) { return templates[0]; }

    float FindXofMax(TGraph *g) {
        Double_t x, y, xMax, yMax;
        g->GetPoint(0, xMax, yMax);

        for (int i = 0; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);
            if (y > yMax) {
                xMax = x;
                yMax = y;
            }
        }

        return xMax;
    }

    void FindMax(TGraph *g, Double_t &xMax, Double_t &yMax) {
        Double_t x, y;
        g->GetPoint(0, xMax, yMax);

        for (int i = 1; i < g->GetN(); i++) {
            g->GetPoint(i, x, y);
            if (y > yMax) {
                xMax = x;
                yMax = y;
            }
        }
    }

    float GetBaseline(TGraph *g) {
        double prevX, prevY;
        double x, y;
        g->GetPoint(0, x, y);
        float sum = y;
        int cnt = 1;

        for (g->GetPoint(cnt, x, y); abs((sum / cnt) - y) < 2.5f; cnt++) {
            sum += y;
            prevX = x;
            prevY = y;
            g->GetPoint(cnt + 1, x, y);
        }

        return sum / cnt;
    }

    double scaledTemplate(double *x, double *par) {
        if (gTemplate)
            return par[0] *
                   gTemplate->GetBinContent(gTemplate->FindBin(x[0] - par[1]));
        else
            return 0;
    }

    Result *Fit(TH1F *h, TGraph *g, TDirectory *dir = nullptr) {
        Double_t xMax, yMax;
        FindMax(g, xMax, yMax);

        Double_t amplitude = yMax;
        Double_t time = xMax;
        Double_t baseline = 0;
        Double_t baselineErr = 0;

        amplitude = amplitude - baseline;
        amplitude *= 1;

        TH1F *t = (TH1F *)FindTemplate(amplitude)->Clone();
        t->SetBinContent(0, 0);
        t->SetBinContent(t->GetSize() - 1, 0);

        double x, y, xStart, xEnd;
        g->GetPoint(0, x, y);
        xStart = x;
        g->GetPoint(g->GetN() - 1, x, y);
        xEnd = x;

        // g->GetPoint(0, x, y);
        // g->SetPoint(g->GetN(), x - 1, 0);

        TF1 *fitFcn = new TF1("fitFcn", new ScaledTemplate(t),
                              t->GetXaxis()->GetXmin() * 0.9,
                              t->GetXaxis()->GetXmax() * 1.1, 3);
        fitFcn->SetParameter(0, amplitude * 1.5);
        fitFcn->SetParameter(1, 150);
        fitFcn->FixParameter(2, 1);

        fitFcn->SetParLimits(0, amplitude, amplitude * 2);
        fitFcn->SetParLimits(1, 0, 300);
        // fitFcn->SetParLimits(2, .8, 1.2);

        int fr = g->Fit(fitFcn, "MRQ", "", 150, 650);
        {
            g->SetTitle("Fitting example");
            TF1 *f = g->GetFunction("fitFcn");
            double x1, y1;
            g->GetPoint(0, x1, y1);
            double shift = x1 - f->GetXmin();

            for (int i = 0; i < g->GetN(); i++) {
                g->GetPoint(i, x1, y1);
                g->SetPoint(i, x1 - shift, y1);
            }
        }

        TF1 *fittedSignal = (TF1 *)g->GetFunction("fitFcn")->Clone();
        Double_t QfromHisto, QfromHistoErr;
        QfromHisto = h->IntegralAndError(1, h->GetXaxis()->GetNbins(),
                                         QfromHistoErr, "width") -
                     baseline * h->GetXaxis()->GetNbins() *
                         h->GetXaxis()->GetBinWidth(1);

        QfromHistoErr = sqrt(pow(QfromHistoErr, 2) +
                             pow(baselineErr * h->GetXaxis()->GetNbins() *
                                     h->GetXaxis()->GetBinWidth(1),
                                 2));

        Double_t QfromFit = fittedSignal->Integral(fittedSignal->GetXmin(),
                                                   fittedSignal->GetXmax());
        Double_t QfromFitErr = QfromFit * fittedSignal->GetParError(0) /
                               fittedSignal->GetParameter(0);

        Result *res = new Result();
        res->graph = QfromFit;
        res->orig = h->Integral();
        res->err = (res->orig - res->graph) / res->orig * 100;
        res->amp = h->GetMaximum();

        if (testMode) {
            TH1F *hCpy = (TH1F *)h->Clone();
            hCpy->SetTitle(Form("%s-err:%.2lf%%", h->GetName(), res->err));

            TileComposer *comp;
            if (abs(res->err) > 20)
                comp = aboveTC;
            else
                comp = belowTC;

            comp->draw(hCpy);

            auto *gCpy = (TGraph *)g->Clone();
            auto *fH = fittedSignal->CreateHistogram();

            double xC, yC;
            gCpy->GetPoint(0, xC, yC);
            double par1 = fittedSignal->GetParameter(1);
            double diff = par1 - xC;
            for (int i = 0; i < gCpy->GetN(); i++) {
                gCpy->GetPoint(i, x, y);
                gCpy->SetPoint(i, x + diff, y);
            }
            // printf("err: %lf, par1: %lf, diff: %lf\n", res->err, par1, diff);
            // gCpy->Print();

            gCpy->SetLineColor(kBlue);
            gCpy->SetMarkerColor(kBlue);
            gCpy->SetMarkerStyle(23);
            comp->draw(gCpy, "P");

            fH->SetLineColor(kRed);
            comp->draw(fH, "same");

            comp->nextFrame();
        }

        res->type = FinderType::Root;

        fittedSignal->SetLineColor(kBlue);
        if (dir) dir->WriteTObject(fittedSignal, "f");

        // t->Delete();
        return res;
    }

    void finalize() override {
        if (testMode) {
            belowTC->close();
            aboveTC->close();
        }
    }

   private:
    Result *consumeInt(TObjectPair<TH1F, TGraph> *p,
                       bool threadSafe = false) override {
        if (p) {
            gSystem->RedirectOutput("/dev/null");
            Result *res = Fit(p->e1, p->e2);
            gSystem->RedirectOutput(nullptr);
            // p->e1->Delete();
            // p->e2->Delete();
            // delete p;
            return res;
        } else
            return nullptr;
    }
};
