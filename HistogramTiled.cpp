#include "Pipeline.hpp"

void HistogramTiled(int n = 1000, int singlePage = 25,
                    const char *name = "histograms") {
    list<const char *> *names = new list<const char *>();
    for (int i = 0; i < 7; i++)
        names->push_back(Form("data/signals%i.root", i));

    Provider<TObjectPair<TH1F, TGraph>> *provider = new Preprocessor(
        new Interpolator(20, new TObjectProvider<TH1F>(names)));

    int nH;
    TH1F **templates = Utils::getTemplates(new TFile("norms6.root"), nH);
    for (int i = 0; i < nH; i++)
        templates[i] = Utils::zeroAlignAndNormalizeInPlace(
            Utils::extractTemplatePeak(templates[i]));

    auto *t = templates[0];
    Utils::alignLeftInPlace(t);

    int cnt = 0, w = TMath::Nint(sqrt(singlePage)), h = singlePage / w;
    TCanvas *c = new TCanvas("c", "", 1200, 1200);
    auto lambda = [&](TObjectPair<TH1F, TGraph> *p) {
        float max = p->e1->GetMaximum();
        // auto *axis = p->e1->GetXaxis();
        // axis->SetLimits(0, 300);

        // printf("%i\n", cnt);
        Utils::alignLeftInPlace(p->e1);
        if (cnt < n) {
            if (cnt % singlePage == 0) {
                if (cnt > 0) {
                    if (cnt == singlePage)
                        c->Print(Form("%s.pdf(", name));
                    else
                        c->Print(Form("%s.pdf", name));
                }

                c->Clear();
                c->Divide(w, h);
            } else if (cnt == n - 1)
                c->Print(Form("%s.pdf)", name));

            c->cd(cnt % singlePage + 1);
            cnt++;

            auto *tC = Utils::scaleHistogram(t, max);
            tC->GetXaxis()->SetLimits(0, 300);
            p->e1->GetXaxis()->SetLimits(0, 300);

            p->e1->SetLineColor(kRed);
            p->e1->DrawClone();
            tC->Draw("same");
        }
        delete p;
        return p;
    };

    auto *consumer = new LambdaConsumer<TObjectPair<TH1F, TGraph>,
                                        TObjectPair<TH1F, TGraph>>(lambda);

    auto *runner = consumer->asTask()->with(provider);
    runner->runVoid();
}