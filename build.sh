#!/bin/bash

g++ -g "$1.cpp" -o "$1.exe" `root-config  --cflags --glibs`
if [ $? -eq 0 ]; then
    echo "Compiled"
    exit 0
else
    echo "Not compiled"
    exit 1
fi