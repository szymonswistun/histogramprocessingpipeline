import java.io._
import java.nio.file._

def grouped[T](l: List[T], cnt: Int = 2): List[List[T]] = {
    l match {
        case a :: b :: r => List(a, b) :: grouped(r)
        case _ => Nil
    }
}

def toFrame(t: (String, List[String])): String = 
    """\clearpage
    \begin{frame}{%s}
        \begin{figure}
            \begin{subfigure}{.45\textwidth}
                \includegraphics[width=\linewidth]{%s}
                \caption{mean}
                \label{fig:simple}
            \end{subfigure}
            \begin{subfigure}{.45\textwidth}
                \includegraphics[width=\linewidth]{%s}
                \caption{sigma}
                \label{fig:ROOT}
            \end{subfigure}
        \end{figure}
\end{frame}""".format(t._1, t._2(0), t._2(1))


val titles = Map(
    "M" -> "Mean method",
    "I" -> "Integral method",
    "R" -> "ROOT method",
    "S" -> "Simple method"
)

val f = new File(".")
val l = f.listFiles.filter(_.getName.endsWith(".png")).map(_.getName)
.map(p => p(4) -> p)
.map(p => (p._1.toString, "slices/" + p._2))
.toList
.groupBy(_._1)
.map(t => (titles(t._1), t._2.map(_._2)))
// grouped(l).foreach(println)
l.foreach(println)
Files.deleteIfExists(Paths.get("latex.out"))

val pWr = new PrintWriter(new FileOutputStream(new File("latex.out")))
l.map(toFrame).foreach(s => pWr.println(s))
pWr.close