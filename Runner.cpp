#include "ResultProducer.cpp"

void Runner() {
    for (int i = 2; i <= 8; i++) {
        printf("Running for %fMHz\n", i * 10.0f);
        // gSystem->RedirectOutput("/dev/null");
        ResultProducer(i * 10.0f);
        // gSystem->RedirectOutput(nullptr);
    }
}