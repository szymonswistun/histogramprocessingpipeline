#include "Abstracts.hpp"
#include "Common.hpp"

class Histogram2Accumulator : public Accumulator<Result, TH2F> {
   public:
    enum HistogramType { AMP, Q };

    Histogram2Accumulator(TH2F* h, HistogramType type) : Accumulator(h) {
        this->type = type;
    }

    template <typename T>
    TerminalConsumer<Result, Result, T>* toTerminalConsumer() {
        return new TerminalConsumer<Result, Result, T>(this);
    }

   private:
    HistogramType type;

    void accumulate(Result* e, bool threadSafe = false) override {
        // printf("Histogram2Accumulator\n");
        if (e == nullptr) return;
        // if (n % 100 == 0) printf("\r%4i|%4i", n, 1000);
        switch (type) {
            case HistogramType::AMP:
                acc->Fill(e->amp, e->err);
                break;
            case HistogramType::Q:
                acc->Fill(e->orig, e->err);
        }
        // printf("\n\n");
    }
};